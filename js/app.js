$(function(){

    $('#formCadastros').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'url_site+api/editaMorador.php';
            urlRedir =  url_site+'?page=consulta';
        } else{
            url = url_site+'api/cadastraMorador.php';
            urlRedir = url_site+'?page=cadastro';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'cadastro');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        });

        return false;
    });
    
        $('#listaClientes').on('click','.removerCliente' ,function(){
            var idRegistro = $(this).attr('data-id');
            $.ajax({
                url: url_site+'api/deletaMorador.php',
                dataType: 'json',
                type: 'POST',
                data: { id: idRegistro},
                success : function(data){
                    if(data.status == 'success'){
                        myAlert(data.status, data.msg, 'main',url_site+'consultaCliente');
                    }else{
                        myAlert(data.status, data.msg, 'main');
                    }
                }
            })
            return false;
        })

    function myAlert(tipo, mensagem, pai, url){
        url = (url == undefined) ? url == '' : url = url;
        componente = '<div class="alert alert-'+ tipo +'" role="alert">'+mensagem+'</div>';

        $(pai).prepend(componente);

        setTimeout(function(){
            $(pai).find('div.alert').remove();

            //vai redirecionar?
            if(tipo == 'success' && url){
                setTimeout(function(){
                    window.location.href = url;
                }, 500);
            }

        }, 1000)
    }

    $('#formCondominio').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaCondominio.php';
            urlRedir = url_site+'?page=consultaCond';
        } else{
            url = url_site+'api/cadastraCond.php';
            urlRedir = url_site+'?page=cadCondominio';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir+'cadCondominio');
                } else{
                    myAlert(data.status, data.msg, 'main',);
                }
            }
        });

        return false;
    })

    $('#listaCondominio').on('click','.removerCliente' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaCondominio.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',url_site+'consultaCond');
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formBlocos').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaBloco.php';
            urlRedir = url_site+'?page=consultaBlocos';
        } else{
            url = url_site+'api/cadastraBloco.php';
            urlRedir = url_site+'?page=cadBlocos';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'consultaBloco');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        });

        return false;
    })

    $('#listaBloco').on('click', '.removerBloco', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: 'api/deletaBloco.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', );
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formUnidades').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaUnidade.php';
            urlRedir = url_site+'?page=consultaUni';
        } else{
            url = url_site+'api/cadastraUnidade.php';
            urlRedir = url_site+'?page=cadUnidades';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'cadUnidades');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        });

        return false;
    })

    $('#listaUni').on('click', '.removerUni', function(){
        var idRegistro = $(this).attr('data-id');

        $.ajax({
            url: 'api/deletaUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#formConselho').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url = '';
        var urlRedir;

        if(editar){
            url = url_site+'api/editaConselho.php';
            urlRedir = url_site+'?page=consultaConselho';
        } else{
            url = url_site+'api/cadastraConselho.php';
            urlRedir = url_site+'?page=cadConselho';
        }

        $('.btnEnviar').attr('disabled', 'disabled');        

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'cadConselho');
                } else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        });

        return false;
    })


    $('#listaConselho').on('click','.removerConselho' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaConselho.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',url_site+'consultaConselho');
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })


    $('.fromCondominio').change(function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: 'api/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado},
            success : function(data){
                selectPopulation('.fromBloco',data.resultSet, 'nomeBloco');
            }
        })

    })

    //chamar unidades após selecionar blocos
    $('.fromBloco').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: 'api/cadastroUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success: function(data){
                selectPopulation('.fromUnidade', data.resultSet, 'numeroUnidade');
            }
        })
    })


    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Selecione</opition>';

        if(dados.length == 1){
            estrutura += '<option value="'+dados.id+'">'+dados[i].nomeBloco+'</option>';
        }else {
            for (let i = 0; i < dados.length; i++) {
            estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>';
        }

        
        
        }
        $(seletor).html(estrutura)
    }
    

    //controlador do filtro
    $('#filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        termo1 = (termo1) ? termo1+'/' : '';
        termo2 = (termo2) ? termo2+'/' : ''

        window.location.href = url_site+pagina+'/busca/'+termo1+termo2

        return false;
    })

});

$('.termo1, .termo2').on('keyup focusout change',function(){
    var termo1 = $('.termo1').val();
    var termo2 = $('.termo2').val();
    if(termo1 || termo2){
        $('button[type="submit"]').prop('disabled', false);
    }else{
        $('button[type="submit"]').prop('disabled', true);
    }
    
});

    $('input[name="telefone"]').mask('(00) 0 0000-0000');
    $('input[name="cpf"]').mask('000.000.000-00');