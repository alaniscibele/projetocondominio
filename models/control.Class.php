<?
Class Control{
    protected $dados = array();
    protected $table;

    function __construct(){
        
    }

    function insert($table, $dados){
        
        try{
            if(!$table){
                Throw new \Exception("Erro encontrado STR".rand(999,9999));
            } else{
                $contagem = array_key_last($_SESSION[$table]) + 1;
        
                foreach($dados as $ch => $campos){
                    $_SESSION[$table][$contagem][$ch] = $dados[$ch];
                }
                
                $_SESSION[$table][$contagem]['dataCadastro'] = date('Y-m-d H:i:s');
            
                return true;
            }
        } catch (\Throwable $e){
            $_SESSION['ERROR']['IDENTIFICADOR'] = $e->getMessage();
            $_SESSION['ERROR']['CODIGO'] = $e->getCode();
            $_SESSION['ERROR']['FILE'] = $e->getFile();
            $_SESSION['ERROR']['LINE'] = $e->getLine();
            return false;
        }
    }

    function update($table, $dados){
        try{
            if(!$table){
                Throw new \Exception("Erro encontrado STR".rand(999,9999));
            } else{
                foreach($_SESSION[$table][$dados['editar']] as $ch => $edit){
                    if($ch != 'dataCadastro' && $ch != 'dataEdicao'){
                        $_SESSION[$table][$dados['editar']][$ch] = $dados[$ch];
                    }
                }
    
                $_SESSION[$table][$dados['editar']]['dataEdicao'] = date('Y-m-d H:i:s');
                return true;
            }
        } catch (\Throwable $e){
            $_SESSION['ERROR']['IDENTIFICADOR'] = $e->getMessage();
            $_SESSION['ERROR']['CODIGO'] = $e->getCode();
            $_SESSION['ERROR']['FILE'] = $e->getFile();
            $_SESSION['ERROR']['LINE'] = $e->getLine();
            return false;
        }
    }

    function delete($table, $id){
        try{
            if(!$table){
                Throw new \Exception("Erro encontrado STR".rand(999,9999));
            } else{
                unset($_SESSION[$table][$id]);
                return true;
            }
        } catch (\Throwable $e){
            $_SESSION['ERROR']['IDENTIFICADOR'] = $e->getMessage();
            $_SESSION['ERROR']['CODIGO'] = $e->getCode();
            $_SESSION['ERROR']['FILE'] = $e->getFile();
            $_SESSION['ERROR']['LINE'] = $e->getLine();
            return false;
        }
    }
}
?>