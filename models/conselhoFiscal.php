<?

Class conselhoFiscal extends CadCondominio{
    protected $id;

    function __construct(){

    }
    

    function setConselho($dadosConselho){
        $values = '';
        $sql = 'INSERT INTO cadastroconselho (';

        foreach($dadosConselho as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }


    function getConselho($id=null){
        $qry = 'SELECT 
        conselho.id, cond.nomeCond, conselho.nome, conselho.cpf, conselho.telefone, conselho.funcao
        
        FROM cadastroconselho conselho
        
        INNER JOIN cadastrocondominio cond ON cond.id = conselho.fromCondominio';
        if($id){
            $qry .= ' WHERE conselho.id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique,3);
    }

    function editConselho($dados){
        return $this->updateData('conselho', $dados);
    }

    function deletaConselho($id){
        return $this->deletar('conselho', $id);
    }

}

?>