<?
Class cadBlocos extends CadCondominio{
    protected $id;

    function __construct(){
        
    }

    function setBlocos($dadosBlocos){
        $values = '';
        $sql = 'INSERT INTO cadastrobloco (';

        foreach($dadosBlocos as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getBlocos($id=null){
        $qry = 'SELECT
        cliente.id, cond.nomeCond, bloco.nomeBloco, bloco.qtdeAndares, bloco.qtdeUni
        FROM cadastrocliente cliente
            
        INNER JOIN cadastrocondominio cond ON cond.id = cliente.fromCondominio
        INNER JOIN cadastrobloco bloco ON bloco.id = cliente.fromBloco
        INNER JOIN cadastrounidade unidade ON unidade.id=cliente.fromUnidade
        WHERE cliente.id';
        if($id){
            $qry .= ' WHERE cond.id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry);
    }


    function editBlocos($dadosBloco){
        
        $sql = 'UPDATE cadastrobloco SET ';

        foreach($dadosBloco as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dadosBloco['editar'];

        return $this->updateData($sql);
    }


    function deletaBloco($id){
        //return $this->deletar('blocos', $id);
        return $this->deletar("DELETE FROM cadastrobloco WHERE id =".$id);
    }

function getBlocoFromCond($cond){
$qry = 'SELECT id, nomeBloco FROM cadastrobloco WHERE fromCondominio = '.$cond;
return $this->listarData($qry);

}
}