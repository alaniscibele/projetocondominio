<?
Class CadUnidades extends cadBlocos{
    protected $id;

    function __construct(){
        
    }

    function setDadosUni($dadosUni){
        $values = '';
        $sql = 'INSERT INTO cadastrounidade (';

        foreach($dadosUni as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }



    function getDadosUni($id=null){
        $qry = 'SELECT
        unidade.id, cond.nomeCond, bloco.nomeBloco, unidade.numeroUnidade, unidade.metragemUnidade, unidade.qntdGaragem
        
        FROM cadastrounidade unidade
        
        INNER JOIN cadastrocondominio cond ON cond.id = unidade.fromCondominio
        INNER JOIN cadastrobloco bloco ON bloco.id = unidade.fromBloco';
        if($id){
            $qry .= ' WHERE id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique,3);
    }




    function editUnidade($dadosUni){
        return $this->updateData('unidades', $dadosUni);
    }

    function deletaUnidade($id){
        return $this->deletar('unidades', $id);
    }



    function getUnidadesFromBloco($id){
        $qry = 'SELECT id, numeroUnidade FROM cadastrounidade WHERE fromBloco = '.$id;
        return $this->listarData($qry);
    }

}

?>