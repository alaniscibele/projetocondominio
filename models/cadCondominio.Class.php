<?

Class CadCondominio extends Dao{
    protected $id;
    
    function __construct(){
        
    }

    function setCondominio($dadosCond){
        $values = '';
        $sql = 'INSERT INTO cadastrocondominio (';

        foreach($dadosCond as $ch=>$value){ 
            $sql .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES ('.rtrim($values, ', ').')';
        return $this->insertData($sql);
    }

    function getCondominio($id=null){
        $qry = 'SELECT * FROM cadastrocondominio';
        if($id){
            $qry .= ' WHERE id= '.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique,3);
    }

    function editCondominio($dadosCond){
        
        $sql = 'UPDATE cadastrocondominio SET ';

        foreach($dadosCond as $ch=>$value){
            if($ch != 'editar'){
                $sql .= "`".$ch."` = '".$value."', ";
            }
        }

        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id='.$dadosCond['editar'];

        return $this->updateData($sql);
    }

    function deletaCondominio($id){
        return $this->deletar('DELETE FROM cadastrocondominio WHERE id='.$id);
    }

}

?>