<?
session_start();
error_reporting(0);

//caminho absoluto
$localDir = 'PHP/projetocondominio/'; //pasta do projeto
$base_url = "http://".$_SERVER['HTTP_HOST'].'/';

$url_site = $base_url.$localDir; //caminho para URL

$fullPath = $_SERVER['DOCUMENT_ROOT'].'/';
$fullPath .= $localDir;

$includes = $fullPath.'includes/';
$models = $fullPath.'models/';
$controllers = $fullPath.'controllers/';
$views = $fullPath.'views/';



require "$models./ConnectDB.class.php";
require "$models./restrito.Class.php";
require "$models./usuario.Class.php";
require "$models./dao.class.php";
require "$models./cadCondominio.Class.php";
require "$models./cadBlocos.Class.php";
require "$models./cadUnidades.Class.php";
require "$models./cadastro.Class.php";
require "$models./conselhoFiscal.php";

define('DEBUG',true);
function legivel($var,$width = '250',$height = '400') {
    if(DEBUG){
        echo "<pre>";
        if(is_array($var)) {
            print_r($var);
        } else {
            print($var);
        }
        echo "</pre>";
    }
}



function dateFormat($d, $tipo = true){ //2022-03-23 16:24:37

    if(!$d){
        return '--';
    }

    if($tipo){
        $hora = explode(' ', $d); //divide em dia[0] e hora[1]
        $data = explode('-', $hora[0]);
        return $data[2].'/'.$data[1].'/'.$data[0].' '.$hora[1];

    } else{

        $hora = explode(' ', $d);
        $data = explode('/', $hora[0]);
        return $data[2].'-'.$data[1].'-'.$data[0].' '.$hora[1];
    }
}

$estados = array( 
    "AC" => "Acre", 
    "AL" => "Alagoas", 
    "AM" => "Amazonas", 
    "AP" => "Amapá",
    "BA" => "Bahia",
    "CE" => "Ceará",
    "DF" => "Distrito Federal",
    "ES" => "Espírito Santo",
    "GO" => "Goiás",
    "MA" => "Maranhão",
    "MT" => "Mato Grosso",
    "MS" => "Mato Grosso do Sul",
    "MG" => "Minas Gerais",
    "PA" => "Pará",
    "PB" => "Paraíba",
    "PR" => "Paraná",
    "PE" => "Pernambuco",
    "PI" => "Piauí",
    "RJ" => "Rio de Janeiro",
    "RN" => "Rio Grande do Norte",
    "RO" => "Rondônia",
    "RS" => "Rio Grande do Sul",
    "RR" => "Roraima",
    "SC" => "Santa Catarina",
    "SE" => "Sergipe",
    "SP" => "São Paulo",
    "TO" => "Tocantins"
);



function trataUrl($params = array()){
    $url = (isset($_GET['b'])) ?'busca/' : '';
    foreach($params as $field => $value){
        $url .=$value.'/';
    }
    return $url;
}

function antiinject($var,$quotes=ENT_NOQUOTES,$keeptags=false) {
    //ENT_QUOTES, ENT_NOQUOTES, ENT_COMPAT;
    
    
    if(!is_array($var)){
        $var = stripslashes($var);
        $var = html_entity_decode($var,$quotes,'utf-8');
        if(!$keeptags) {
            $var = strip_tags($var);
        }
        $var = trim($var);
        //$var = utf8_decode($var);
        /**/
        $var = htmlentities($var,$quotes);
        if($keeptags) {
            $var = str_replace('&lt;','<',$var);
            $var = str_replace('&gt;','>',$var);
        }
        /**/
        $var = addslashes($var);
    } else {
        foreach($var as $k=>$ar){
            $var[$k] = antiinject($ar);
        }
    }
    return $var;
}
?>