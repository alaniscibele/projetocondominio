<body style="background-color:#FAE7C5">

<? 
include "uteis.php";

$user = new Restrito();
if(!$user->acesso()){
    header("Location: login.php");
}
if($_GET['page'] == 'logout'){
    if ($user->logout()){
        header('location: '.$url_site.'login.php');

    }
}

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$url_site?>css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Gestão Cliente</title>
</head>

<body>

<nav class="navbar navbar-expand-lg mb-4" style="background-color:#6c7c94">
        <a class="navbar-brand" href="#"><i class="bi bi-columns-gap" style="font-size: 25px; color: white"></i> <span class="text-white font-weight-bold pl-3"> GESTÃO DE CLIENTES </span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end mr-5" id="navbarNav">
            <ul class="navbar-nav float-right">
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>consultaCliente">Lista Clientes</a>
                </li> 
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>cadastro">Cadastrar Cliente</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>consultaCond">Lista Condomínios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>cadCondominio">Cadastrar Condomínio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>consultaBlocos">Lista Blocos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>cadBlocos">Cadastrar Blocos</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>consultaConselho">Lista Conselho</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>cadConselho">Cadastrar Conselho</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>consultaUni">Lista Unidade</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="<?=$url_site?>cadUnidades">Cadastrar Unidade</a>
                </li><?
                $nome = explode(' ',$_SESSION['USUARIO']['nome'])
                ?>
                <small class="text-light">Olá <?=$nome[0];?>, seja bem vindo(a).</smal>
            
                        <li class="nav-item config"><a href="<?=$url_site?>/logout"  class="nav-link text-light">
                    <i class="bi bi-box-arrow-right"></i></a>
                </li>
            </ul>
        </div>
    </nav>

    <main class="container">
    <?
        switch ($_GET['page']) {
            case '':
            case 'inicio':
                require "controllers/inicio.php";
                require "views/inicio.php";
            break;
            
            default:
                require 'controllers/'.$_GET['page'].'.php';
                require 'views/'.$_GET['page'].'.php';
            break;
        }
    ?>
    </main>


    <footer class="fixed-bottom" style="background-color:#6c7c94">
        <div class="w-100 py-2 px-2 bg-secondary text-white tx-small"><small>&copy; Todos os direitos reservados.</small></div>
    </footer>


    <script> var url_site='<?=$url_site?>';</script>
    <script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.bundle.min.js"></script>    
    <script src="<?=$url_site?>js/jquery.mask.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.min.js"></script>
    <script src="<?=$url_site?>js/app.js?v=<?=rand(0,9999)?>"></script>
</body>

</html>