<center>
    <h1 class="text-center text-dark mb-5">Cadastros dos moradores</h1>
    <div class="row">
        <div class="col-12 text-dark">
            <form action="" method="post" id="formCadastros">
                <div class="form-group col-md-6">
                    <label for="name">Nome*</label>
                    <input type="text" name="nome" class="form-control" id="name" aria-describedby="name" value="<?= $morador['nome'] ?>" required>
                </div>

                <div class=" text-dark form-group col-12 col-md-6">
                    <label for="cpf">CPF*</label>
                    <input type="text" name="cpf" class="form-control" id="cpf" placeholder="000.000.000-00" value="<?= $morador['cpf'] ?>" required>
                </div>

                <div class="form-group col-12 col-md-6">
                    <label for="telefone">Telefone</label>
                    <input type="text" name="telefone" class="form-control" id="telefone" aria-describedby="telefone" value="<?= $morador['telefone'] ?>" required>
                </div>

                <div class="form-group col-12 col-md-6">
                    <label for="email">E-mail*</label>
                    <input type="text" name="email" class="form-control" id="email" aria-describedby="email" value="<?= $morador['email'] ?>" required>
                </div>

                <div class="form-group col-12">
                    <label for="condominio">Condomínio*</label>

                    <select name="fromCondominio" class="custom-select fromCondominio" style="width:30%; margin: 0 23rem">
                        <option value="">Select</option>
                        <?

                        //legivel($listaCond);
                        foreach ($listaCond['resultSet'] as $valor) { ?>
                            <option value="<?= $valor['id'] ?>" <?= ($valor['id'] == $morador['fromCondominio'] ? 'selected' : '') ?>><?= $valor["nomeCond"] ?></option>
                        <? } ?>

                    </select>
                </div>

                <div class="form-group col-12">
                    <label for="bloco">Bloco*</label>
                    <select name="fromBloco" id="fromBloco" class="custom-select fromBloco" style="width:30%; margin: 0 23rem">
                        <?
                        if ($_GET['id']) {
                            $blocos = $moradores->getBlocoFromCond($morador['fromCondominio']);
                            foreach ($blocos['resultSet'] as $bloco) {
                        ?>
                                <option value="<?= $bloco['id'] ?>" <?= ($bloco['id'] == $morador['fromBloco'] ? 'selected' : '') ?>><?= $bloco['nomeBloco'] ?></option>
                        <? }
                        } ?>

                    </select>


                </div>



                <div class="form-group col-12 col-md-6" style="width:30%; margin: 0 23rem">
                    <label for="unidade">Unidade*</label>
                    <select name="fromUnidade" id="fromUnidade" class="custom-select fromUnidade">

                        <?
                        if ($_GET['id']) {
                            $unidades = $moradores->getUnidadesFromBloco($morador['fromBloco']);
                            foreach ($unidades['resultSet'] as $unidade) {
                        ?>
                                <option value="<?= $unidade['id'] ?>" <?= ($unidade['id'] == $morador['fromUnidade'] ? 'selected' : '') ?>><?= $unidade['numeroUnidade'] ?></option>
                        <? }
                        } ?>

                    </select>
                </div>


                <? if ($_GET['id']) { ?>
                    <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
                <? } ?>
                <br><br><br>
                <button type="submit" class="btn btn-dark btnEnviar col-12 col-sm-1 ml-3 mb-3">Enviar</button>
                <a href="index.php?page=consultaCliente" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar blocos"><i class="bi bi-clipboard2-data" style="font-size: 38px"></i>Lista</a>

            </form>
        </div>

    </div>
</center>
<br><br><br>
<div>.</div>