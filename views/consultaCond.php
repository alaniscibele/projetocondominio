<h1 class="text-center text-dark mb-4">Consulta dos condomínios</h1>

<div class="row">
    <div class="col-12 text-dark">
        <table class="table text-center" id="listaCond">
            <thead>
                <tr style="background-color:#a27b90 ; color:#fff">
                    <th scope="col">Condomínio</th>
                    <th scope="col">Síndico</th>
                    <th scope="col">Quantidade de Blocos</th>
                    <th scope="col">Endereço</th>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $key=>$cond){?>    
                    <tr data-id="<?=$cond['id']?>">
                        <td style="background-color:#DBC7D7" ><?=$cond['nomeCond']?></td>
                        <td style="background-color:#DBC7D7" ><?=$cond['nomeSindico']?></td>
                        <td style="background-color:#DBC7D7" ><?=$cond['qtdeBlocos']?></td>
                        <td style="background-color:#DBC7D7" >Rua <?=$cond['logradouro']?>, <?=$cond['numero']?> - <?=$cond['bairro']?> - <?=$cond['cidade']?>/<?=$cond['estado']?> - <?=$cond['cep']?>
                        </td>
                        <td style="background-color:#DBC7D7" ><a href="index.php?page=cadCondominio&id=<?=$cond['id']?>" class="text-dark"><i class="bi bi-pencil-square"></i></a></td>
                        <td style="background-color:#DBC7D7" ><a href="#" data-id="<?=$cond['id']?>" class="text-dark removerCond"><i class="bi bi-trash-fill"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="5" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=(($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                </tr>
                
            </tbody>
        </table>
    <center><?=$condominio->renderPagination($result['qtPaginas'])?></center>
</div>
</div>
<br><br><br>
<div>.</div>