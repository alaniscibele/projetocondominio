<?
// require "classes/control.Class.php";
// require "classes/cadCondominio.Class.php";
// require "classes/cadBlocos.Class.php";



?>


<h1 class="mb-4 mt-3 text-dark text-center">Cadastro de Blocos</h1>

<div class="row">
    <div class="col-12 text-dark">
        <form action="" method="post" id="formBlocos">

            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="condBloco" style="width: 100%; text-align: center">Condomínio*</label>
                <div class="col-12 col-md-12">
                    <select name="fromCondominio" class="custom-select" style="width:30%; margin: 0 23rem">
                        <option value="">Select</option>
                        <?
                        
                        foreach($listaCond['resultSet'] as $valor){?>
                            <option value="<?=$valor['id']?>"><?=$valor["nomeCond"]?></option> 
                        <?}?>

                    </select>
                </div>
            </div>

            <div class="form-group col-12 text-center" style="width: 100%; ">
                <label for="nomeBloco">Nome do bloco*</label>
                <input type="text" name="nomeBloco" class="form-control" id="nomeBloco" value="<?=$popular['nomeBloco']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center " style="width: 100%;">
                <label for="qtdeAndares">Quantidade de andares*</label>
                <input type="text" name="qtdeAndares" class="form-control" id="qtdeAndares" value="<?=$popular['quantidadeAndar']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="qtdeUni">Quantidade de unidades por andar*</label>
                <input type="text" name="qtdeUni" class="form-control" id="qtdeUni" value="<?=$popular['quantidadeUnidade']?>" style="width:50%; margin: 0 270px" require>
            </div>
        
            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>

            <div class="col-12 text-center">
                <button type="submit" class="btn btn-dark btnEnviar col-12 ml-3 mb-3" style="width: 10%;">Enviar</button>
                <a href="index.php?page=consultaBlocos" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar condominios"><i class="bi bi-clipboard2-data" style="font-size: 2rem"></i>Lista</a>
            </div>
        </form>
    </div>
</div>