

<h1 class="text-center text-dark mb-4">Consulta Bloco</h1>

<div class="row">
    <div class="col-12 text-dark">


        <table class="table text-dark text-center" id="listaBloco">
            <thead>
                <tr>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Condomínio</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Nome do Bloco</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Quantidade de Andares</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Unidades por Andar</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Editar</td>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>
                <?
                //legivel($blocos->getBlocos());

                foreach($blocos->getBlocos()['resultSet'] as $key => $bloco){?>
                    
                    <tr data-id="<?=$key?>">
                        <td style="background-color:#DBC7D7"><?=$bloco['nomeCond']?></td>
                        <td style="background-color:#DBC7D7"><?=$bloco['nomeBloco']?></td>
                        <td style="background-color:#DBC7D7"><?=$bloco['qtdeAndares']?></td>
                        <td style="background-color:#DBC7D7"><?=$bloco['qtdeUni']?></td>
                        
                        <td style="background-color:#DBC7D7"><a href="index.php?page=cadBlocos&id=<?=$key?>" class="text-dark"><i class="bi bi-pencil"></i></a></td>
                        <td style="background-color:#DBC7D7"><a href="#" data-id="<?=$key?>" class="text-dark deletaBloco"><i class="bi bi-trash3"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="5" class="text-right"> <b>Total de Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=(($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<br><br><br>
<div>.</div>