<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>


<h1 class="text-center text-dark mb-4">Consulta do Conselho Fiscal</h1>

<div class="row">
    <div class="col-12 text-dark">
        <table class="table text-center" id="listaConselho">
            <thead>
                <tr>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Condomínio</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Nome</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">CPF</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Telefone</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Função</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Editar</td>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                <?
                foreach($result['resultSet'] as $key => $cons){?>
                    
                    <tr data-id="<?=$cons['id']?>">
                        <td style="background-color:#DBC7D7"><?=$cons['nomeCond']?></td>
                        <td style="background-color:#DBC7D7"><?=$cons['nome']?></td>
                        <td style="background-color:#DBC7D7"><?=$cons['cpf']?></td>
                        <td style="background-color:#DBC7D7"><?=$cons['telefone']?></td>
                        <td style="background-color:#DBC7D7"><?=$cons['funcao']?></td>
                        <td style="background-color:#DBC7D7"><a href="index.php?page=cadConselho&id=<?=$cons['id']?>" class="text-dark"><i class="bi bi-pencil"></i></a></td>
                        <td style="background-color:#DBC7D7"><a href="#" data-id="<?=$cons['id']?>" class="text-dark removerConselho"><i class="bi bi-trash3"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="6" class="text-right"> <b>Total Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=(($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                </tr>
            </tbody>
            
        </table>
<center><?=$conselho->renderPagination($result['qtPaginas'])?></center>

</body>