
<h1 class="text-center text-dark mb-4">Consulta Bloco</h1>

<div class="row">
    <div class="col-12 text-dark">


        <table class="table text-center" id="listaMorador">
            <thead>
                <tr>
                    <th scope="col">Condomínio</th>
                    <th scope="col">Nome do Bloco</th>
                    <th scope="col">Nome Unidade</td>
                    <th scope="col">Nome Morador</td>
                    <th scope="col">CPF</td>
                    <th scope="col">E-mail</td>
                    <th scope="col">Telefone</td>
                    <th scope="col" title="Data Cadastro"><i class="bi bi-calendar-plus" style="font-size: 25px;"></i></td>
                    <th scope="col" title="Data Última Edição"><i class="bi bi-calendar-check" style="font-size: 25px;"></i></td>
                    <th scope="col">Editar</td>
                    <th scope="col">Excluir</td>
                    
                </tr>
            </thead>

            <tbody>
                <?
                foreach($result['resultSet'] as $key => $dados){?>
                    
                    <tr data-id="<?=$dados["id"]?>">
                        <td><?=$dados['nomeCond']?></td>
                        <td><?=$dados['nomeBloco']?></td>
                        <td><?=$dados['numeroUnidade']?></td>
                        <td><?=$dados['nome']?></td>
                        <td><?=$dados['cpf']?></td>
                        <td><?=$dados['email']?></td>
                        <td><?=$dados['telefone']?></td>
                        
                        <td><a href="index.php?page=cadBlocos&id=<?=$dados["id"]?>" class="text-dark"><i class="bi bi-pencil"></i></a></td>
                        <td><a href="#" data-id="<?=$dados["id"]?>" class="text-dark removerBloco"><i class="bi bi-trash3"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="5" class="text-right"> <b>Total de Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=(($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'])?></td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
<br><br><br>
<div>.</div>