<h1 class="text-center text-dark mb-4">Consulta Cliente</h1>

<div class="row">
    <div class="col-12 text-dark">
    <tr>
        <td colspan="5">
            <form class="form-inline my-2 my-lg-0" method="GET" id="filtro">
                <input type="hidden" name="page" value="consultaCliente">
                <input class="form-control mr-sm-2 termo1" type="search" placeholder="Buscar por Nome" aria-label="Search" name="b[nome]">
                <select name="b[fromCondominio]" class="custom-select termo2">
                    <option value="">Buscar por Condominio</option>
                <?
                foreach($listCondominio['resultSet'] as $condominios){
                    echo '<option value="'.$condominios['id'].'">'.$condominios['nomeCond'].'</option>';
                } ?>
                </select>
                <div style="color:#FAE7C5">..</div>
                <button class="btn btn-outline-dark my-2 my-sm-0" type="submit" disabled style="background-color:#CCDCE1">Buscar</button>
                <div style="color:#FAE7C5">..</div>
                <a class="btn btn-outline-dark my-2 my-sm-0" href="<?=$url_site?>consultaCliente" style="background-color:#CCDCE1">Limpar</a>

            </form>
        </td>
    </tr>
    <br>
        <?
        $listar = '<table class="table text-dark text-center" id="listaClientes">';
            $listar .= '<thead>';
                $listar .= '<tr style="background-color:#a27b90 ; color:#fff">';
                    $listar .= '<th scope="col">Nomes</td>';
                    $listar .= '<th scope="col">CPF</td>';
                    $listar .= '<th scope="col">E-mail</td>';
                    $listar .= '<th scope="col">Telefone</td>';
                    $listar .= '<th scope="col" title="Data Cadastro"><i class="bi bi-clock-fill" style="font-size: 25px;"></i></td>';
                    $listar .= '<th scope="col" title="Data Última Edição"><i class="bi bi-clock" style="font-size: 25px;"></i></td>';
                    $listar .= '<th scope="col">Editar</td>';
                    $listar .= '<th scope="col">Excluir</td>';
                $listar .= '</tr>';
            $listar .= '</thead>';
            
            $listar .= '<tbody>';



            //legivel($cliente -> getMorador());
                foreach($result['resultSet'] as $cli){
                    $listar .= '<tr data-id="'.$cli['id'].'">';
                        $listar .= '<td style="background-color:#DBC7D7" >'.$cli['nome'].'</td>';
                        $listar .= '<td style="background-color:#DBC7D7" >'.$cli['cpf'].'</td>';
                        $listar .= '<td style="background-color:#DBC7D7" >'.$cli['email'].'</td>';
                        $listar .= '<td style="background-color:#DBC7D7" >'.$cli['telefone'].'</td>';
                        $listar .= '<td style="background-color:#DBC7D7" >'.dateFormat($cli['dataCadastro']).'</td>';
                        $listar .= '<td style="background-color:#DBC7D7" >'.dateFormat($cli['dataEdicao']).'</td>';
                        $listar .= '<td style="background-color:#DBC7D7" ><a href="index.php?page=cadastro&id='.$cli['id'].'" class="text-dark"><i class="bi bi-pencil"></i></a></td>';
                        $listar .= '<td style="background-color:#DBC7D7" ><a href="#" data-id="'.$cli['id'].'" class="text-dark removerCliente"><i class="bi bi-trash3"></i></a></td>';
                    $listar .= '</tr>';
                }
                $listar .= '<tr>';
                
                $listar .= '</tr>';

            $listar .= '</tbody>';
        $listar .= '</table>';

        echo $listar;
        ?>
    </div>

    <div class="col-sm-12">
    <?=$paginacao;?>
    <?=$totalRegistros;?>
</div>
</div>
<br><br><br>
<div>.</div>


