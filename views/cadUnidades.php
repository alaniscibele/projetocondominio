<?




?>

<h1 class="mb-4 mt-3 text-dark text-center">Cadastro de Unidades</h1>

<div class="row">
    <div class="col-12 text-dark">
        <form action="" method="post" id="formUnidades">

            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="fromCondominio" style="width: 100%; text-align: center">Condomínio*</label>
                <div class="col-12 col-md-12">

                <select name="fromCondominio" class="custom-select" style="width:30%; margin: 0 23rem">
                        <option value="">Select</option>
                        <?

                        //legivel($listaCond);
                        foreach($listaCond['resultSet'] as $valor){?>
                            <option value="<?=$valor['id']?>"><?=$valor["nomeCond"]?></option> 
                        <?}?>

                    </select>


                </div>
            </div>

            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="blocoUni" style="width: 100%; text-align: center">Bloco*</label>
                <div class="col-12 col-md-12">


                <select name="fromBloco" id="fromBloco" class="custom-select" style="width:30%; margin: 0 23rem">
                        <option value="">Select</option>
                        <?

                        //legivel($listaBloco);
                        foreach($listaBloco['resultSet'] as $valor){?>
                            <option value="<?=$valor['id']?>"><?=$valor["nomeBloco"]?></option> 
                        <?}?>

                    </select>
                </div>
            </div>

            <div class="form-group col-12 text-center" style="width: 100%; ">
                <label for="numUnidade">Número da unidade*</label>
                <input type="text" name="numeroUnidade" class="form-control" id="numeroUnidade" value="<?=$popular['numeroUnidade']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center " style="width: 100%;">
                <label for="metroUnidade">Metragem da unidade*</label>
                <input type="text" name="metragemUnidade" class="form-control" id="metragemUnidade" value="<?=$popular['metragemUnidade']?>" style="width:50%; margin: 0 270px" require>
            </div>
            
            <div class="form-group col-12 text-center" style="width: 100%;">
                <label for="qtdeVagas">Quantidade de vagas na garagem*</label>
                <input type="text" name="qntdGaragem" class="form-control" id="qtdeVagas" value="<?=$popular['qntdGaragem']?>" style="width:50%; margin: 0 270px" require>
            </div>
        
            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>

            <div class="col-12 text-center">
                <button type="submit" class="btn btn-dark btnEnviar col-12 ml-3 mb-3" style="width: 10%;">Enviar</button>
                <a href="index.php?page=consultaUni" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar blocos"><i class="bi bi-journal-text" style="font-size: 2rem"></i></a>
            </div>
        </form>
    </div>
</div>