

<center>
    
<h1 class="text-center text-dark mb-5">Cadastro Conselho Fiscal</h1>
<div class="row">
    <div class="col-12 text-dark">
        <form action="" method="post" id="formConselho">
            <div class="form-group col-md-6">
                <label for="name">Nome*</label>
                <input type="text" name="nome" class="form-control" id="name" aria-describedby="name" value="<?=$popular['nome']?>" required>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="cpf">CPF*</label>
                <input type="text" name="cpf" class="form-control" id="cpf" placeholder="                                             000.000.000-00" value="<?=$popular['cpf']?>" required>
            </div>
            
            <div class="form-group col-12 col-md-6">
                <label for="telefone">Telefone</label>
                <input type="text" name="telefone" class="form-control" id="telefone" aria-describedby="telefone" value="<?=$popular['telefone']?>" required>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="funcao">Função*</label>

                <select name="funcao" id="funcao" class="custom-select">
                    <option value="">Select</option>
                    <option value="subsindico">Subsíndico</option>
                    <option value="conselheiro">Conselheiro</option>
                </select>
            </div>

            <div class="form-group col-12 col-md-6">
                <label for="conselhoCond">Condomínio*</label>
                
                <select name="fromCondominio" class="custom-select" style="width:30%; margin: 0 23rem">
                        <option value="">Select</option>
                        <?

                        //legivel($listaCond);
                        foreach($listaCond['resultSet'] as $valor){?>
                            <option value="<?=$valor['id']?>"><?=$valor["nomeCond"]?></option> 
                        <?}?>

                    </select>

            </div>

            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>

            <button type="submit" class="btn btn-dark btnEnviar col-12 col-sm-1 ml-3 mb-3">Enviar</button>
            <a href="index.php?page=consultaConselho" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar condominios"><i class="bi bi-clipboard2-data" style="font-size: 2rem"></i>Lista</a>
        </form>
    </div>
</div>
</center>