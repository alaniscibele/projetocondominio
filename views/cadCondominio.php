<?



?>

<center>
<h1 class=" text-dark text-center mb-5">Cadastros dos condomínios</h1>
<div class="row">
    <div class="col-12 text-dark">

        <form action="" method="post" id="formCondominio">
        <input type="hidden" name="fromAdministradora" value="1">

            <div class="form-group col-md-6 d-inline-block">
                <label for="nameCond">Nome do condomínio*</label>
                <input type="text" name="nomeCond" class="form-control" id="nomeCond" aria-describedby="nameCond" value="<?=$popular['nomeCond']?>" required>
            </div>
            
            <div class="form-group col-12 col-md-3 d-inline-block">
                <label for="qtdeBlocos">Quantidade de blocos*</label>
                <input type="text" name="qtdeBlocos" class="form-control" id="qtdeBlocos" aria-describedby="qtdeBlocos" value="<?=$popular['qtdeBlocos']?>" required>
            </div>

            <div class="form-group col-12 col-md-6 d-inline-block">
                <label for="nomeSindico">Nome do Síndico*</label>
                <input type="text" name="nomeSindico" class="form-control" id="nomeSindico" value="<?=$popular['nomeSindico'] ?>" required>
            </div>

            <h2 class="mb-3 mt-3 ml-2 text-center">Endereço</h2>

            <div class="form-group col-12 col-md-6 d-inline-block">
                <label for="logradouro">Logradouro*</label>
                <input type="text" name="logradouro" class="form-control" id="logradouro" aria-describedby="logradouro" value="<?=$popular['logradouro']?>" required>
            </div>

            <div class="form-group col-12 col-md-2 d-inline-block">
                <label for="numero">Número*</label>
                <input type="text" name="numero" class="form-control" id="numero" aria-describedby="numero" value="<?=$popular['numero']?>" required>
            </div>

            <div class="form-group col-12 col-md-3 d-inline-block">
                <label for="bairro">Bairro*</label>
                <input type="text" name="bairro" class="form-control" id="bairro" aria-describedby="bairro" value="<?=$popular['bairro']?>" required>
            </div>

            <div class="form-group col-12 col-md-6 d-inline-block">
                <label for="cidade">Cidade*</label>
                <input type="text" name="cidade" class="form-control" id="cidade" aria-describedby="cidade" value="<?=$popular['cidade']?>" required>
            </div>

            <div class="form-group col-12 col-md-2 d-inline-block">
                <label for="estado" style="width: 100%; text-align: center">UF*</label>
                <div class="col-12 col-md-12">
                    <select name="estado" id="estado" style="width: 100%;">
                        <option value="">Select</option>
                        <?foreach($estados as $ch=>$valor){?>
                            <option value="<?=$ch?>" <?=($ch == $endereco['estado'] ? 'selected="selected"' : '') ?>> <?=$valor?> </option>
                        <?}?>
                    </select>
                </div>
            </div>

            <div class="form-group col-12 col-md-3 d-inline-block">
                <label for="cep">CEP*</label>
                <input type="text" name="cep" class="form-control" id="cep" aria-describedby="cep" value="<?=$popular['cep']?>" required>
            </div>

            <? if($_GET['id']){ ?>
                <input type="hidden" name="editar" value="<?=$_GET['id']?>">
            <? } ?>

            <button type="submit" class="btn btn-dark btnEnviar col-12 col-sm-1 ml-3 mb-3">Enviar</button>
            <a href="index.php?page=consultaCond" class="col-12 col-sm-5 text-dark" style="padding-right: 31px" title="Consultar condominios"><i class="bi bi-clipboard2-data" style="font-size: 2rem"></i>Lista</a>
        </form>
    </div>
</div>
</center>