<table class="col col-12 table table-striped mt-5" id="listaClientes">
    <tr>
        <td colspan="5">
            <form class="form-inline my-2 my-lg-0" action="index.php" method="GET">
                <input type="hidden" name="page" value="listaMoradores">
                <div class="input-group-prepend">
                    <div class="input-group-text">busca por nome</div>
                </div>
                <input class="form-control mr-sm-2" type="search" placeholder="Buscar por nome" aria-label="Search" name="b[nome]">
                <div class="input-group-prepend">
                    <div class="input-group-text">por condomínio</div>
                </div>
                <select name="b[from_condominio]" class="custom-select">
                    <option value="">...</option>
                <?
                $condominio = new CadCondominio();
                $listCondominio = $condominio->getCondominio();
                foreach($listCondominio['resultSet'] as $condominios){
                    echo '<option value="'.$condominios['id'].'">'.$condominios['nomeCondominio'].'</option>';
                } ?>
                </select>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">buscar</button>
                <a class="btn btn-outline-danger my-2 my-sm-0" href="index.php?page=listaMoradores">LIMPAR</a>
            </form>
        </td>
    </tr>
    <tr >
        <td>Nome</td>
        <td>Documento</td>
        <td>Email</td>
        <td>DT. Cadastro</td>
        <td align="center"><a href="index.php?page=cadastro" class="btn btn-primary btn-sm">ADICIONAR</a></td>
    </tr>
    <? 
        $morador = new Cadastro;
        $morador->pagination = 4;
        $morador->busca = $_GET['b'];
        $result = $morador->getMorador();
        //legivel($result);
        foreach ($result['resultSet'] as $dados) { 
    ?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nome'] ?></td>
            <td><?= $dados['cpf'] ?></td>
            <td><?= $dados['email'] ?></td>
            <td><?=dateFormat($dados['dataCadastro'])?></td>
            <td align="center">
                <a href="<?=$url_site?>cadastroMoradores/id/<?=$dados['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a href="#" data-id="<?=$dados['id']?>" class="removerCliente"><i class="bi bi-trash-fill"></i></a>
            </td>
        </tr>
    <? } ?>

</table>
<div class="row">
    <?=($result['totalResults'] > $morador->pagination) ? $morador->renderPagination($result['qtPaginas']) : ''?>
    <div class="totalRegistros col-sm-6">Total Registros <?=($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults']?></div>
</div>

<?
    if(!empty($_GET['deletar'])){
        unset($_SESSION['cadastro'][$_GET['deletar']]);
        header("Location: index.php?page=clientes");
    }
?>