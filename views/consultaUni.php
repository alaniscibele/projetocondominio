<h1 class="text-center text-dark mb-4">Consulta das Unidades</h1>

<div class="row">
    <div class="col-12 text-dark">
        <table class="table text-center" id="listaUni">
            <thead>
                <tr>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Condomínio</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Bloco</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Número da Unidade</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Metragem</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Vagas na Garagem</th>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Editar</td>
                    <th style="background-color:#a27b90 ; color:#fff" scope="col">Excluir</td>
                </tr>
            </thead>

            <tbody>

                

                <? foreach($unidades -> getDadosUni() as $key => $uni){?>
                    
                    <tr data-id="<?=$key?>">
                        <td style="background-color:#DBC7D7"><?=$uni['nomeCond']?></td>
                        <td style="background-color:#DBC7D7"><?=$uni['nomeBloco']?></td>
                        <td style="background-color:#DBC7D7"><?=$uni['numeroUnidade']?></td>
                        <td style="background-color:#DBC7D7"><?=$uni['metragemUnidade']?></td>
                        <td style="background-color:#DBC7D7"><?=$uni['qntdGaragem']?></td>
                        <td style="background-color:#DBC7D7"><a href="index.php?page=cadUnidades&id=<?=$key?>" class="text-dark"><i class="bi bi-pencil"></i></a></td>
                        <td style="background-color:#DBC7D7"><a href="#" data-id="<?=$key?>" class="text-dark removerUnidade"><i class="bi bi-trash3"></i></a></td>
                    </tr>
                <?}?>

                <tr>
                    <td colspan="6" class="text-right"> <b>Total de Registros:</b></td>
                    <td colspan="1" class="text-center totalRegistros"><?=((count($_SESSION['unidades']) < 10) ? '0'.count($_SESSION['unidades']) : count($_SESSION['unidades']))?></td>
                </tr>

            </tbody>
            <center><?=$conselho->renderPagination($result['qtPaginas'])?></center>
        </table>
        <br><br><br>
<div>.</div>