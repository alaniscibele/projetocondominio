-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para projetocondominio
CREATE DATABASE IF NOT EXISTS `projetocondominio` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `projetocondominio`;

-- Copiando estrutura para tabela projetocondominio.administradoras
CREATE TABLE IF NOT EXISTS `administradoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '0',
  `cpf` varchar(40) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.administradoras: ~7 rows (aproximadamente)
DELETE FROM `administradoras`;
/*!40000 ALTER TABLE `administradoras` DISABLE KEYS */;
INSERT INTO `administradoras` (`id`, `nome`, `cpf`) VALUES
	(1, 'Renan e Juan Buffet Ltda', '56321456922'),
	(2, 'Milena e Benjamin Esportes ME', '04588963040'),
	(3, 'Leandro e Manuel Filmagens ME', '02566395877'),
	(4, 'Trend Condominios', '02366958699'),
	(5, 'Bella Vista Construções', '20366901870'),
	(6, 'Santa Anna', '25639568853'),
	(7, 'Joanna Construtora', '11478503690'),
	(8, 'Vitoria Certa', '02369985748'),
	(9, 'Teste', '02122356999');
/*!40000 ALTER TABLE `administradoras` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.cadastrobloco
CREATE TABLE IF NOT EXISTS `cadastrobloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromCondominio` int(11) NOT NULL DEFAULT 0,
  `nomeBloco` varchar(50) NOT NULL,
  `qtdeAndares` varchar(50) NOT NULL,
  `qtdeUni` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chavecond` (`fromCondominio`),
  CONSTRAINT `chavecond` FOREIGN KEY (`fromCondominio`) REFERENCES `cadastrocondominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.cadastrobloco: ~8 rows (aproximadamente)
DELETE FROM `cadastrobloco`;
/*!40000 ALTER TABLE `cadastrobloco` DISABLE KEYS */;
INSERT INTO `cadastrobloco` (`id`, `fromCondominio`, `nomeBloco`, `qtdeAndares`, `qtdeUni`) VALUES
	(1, 8, 'A', '5', '3'),
	(2, 3, 'B', '3', '2'),
	(3, 35, 'A', '1', '4'),
	(4, 2, 'Bloco Legal', '7', '3'),
	(5, 1, 'Bloco Legalllllllll', '7', '2'),
	(6, 1, 'Bloco Legal 2', '7', '5'),
	(7, 36, 'Bloco Flores', '7', '6'),
	(8, 2, 'Bloco Legall', '7', '2');
/*!40000 ALTER TABLE `cadastrobloco` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.cadastrocliente
CREATE TABLE IF NOT EXISTS `cadastrocliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromCondominio` int(11) NOT NULL DEFAULT 0,
  `fromBloco` int(11) NOT NULL DEFAULT 0,
  `fromUnidade` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL,
  `telefone` bigint(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`fromCondominio`),
  KEY `chBloco` (`fromBloco`),
  KEY `FK_cadastrocliente_cadastrounidade` (`fromUnidade`),
  CONSTRAINT `FK_cadastrocliente_cadastrounidade` FOREIGN KEY (`fromUnidade`) REFERENCES `cadastrounidade` (`id`),
  CONSTRAINT `chBloco` FOREIGN KEY (`fromBloco`) REFERENCES `cadastrobloco` (`id`),
  CONSTRAINT `chCondominio` FOREIGN KEY (`fromCondominio`) REFERENCES `cadastrocondominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.cadastrocliente: ~14 rows (aproximadamente)
DELETE FROM `cadastrocliente`;
/*!40000 ALTER TABLE `cadastrocliente` DISABLE KEYS */;
INSERT INTO `cadastrocliente` (`id`, `fromCondominio`, `fromBloco`, `fromUnidade`, `nome`, `cpf`, `telefone`, `email`, `dataCadastro`) VALUES
	(2, 1, 1, 1, 'Luanna dos Santos', '74602985606', 49658412356, 'lua@email.com', '2022-04-01 16:31:43'),
	(3, 7, 1, 3, 'Kamila Fernandes', '81002035609', 47023658916, 'kamila@email.com', '2022-04-01 16:31:13'),
	(5, 1, 3, 1, 'carla', '256.369.158', 0, 'email@email.com', '2022-04-01 14:44:52'),
	(6, 7, 4, 1, 'maria', '589.698.659', 0, 'maria@email.com', '2022-04-01 14:59:49'),
	(7, 30, 2, 2, 'julia', '589.698.659', 0, 'maria@email.com', '2022-04-01 16:31:49'),
	(8, 2, 2, 4, 'testee12356789', '23655896500', 0, 'maria@email.com', '2022-04-04 09:18:38'),
	(9, 2, 2, 2, 'carla', '23655896500', 47895632658, 'email@email.com', '2022-04-04 10:06:55'),
	(10, 2, 2, 2, 'anna camila soares', '23655896500', 47895632658, 'email@email.com', '2022-04-04 11:04:15'),
	(11, 2, 2, 2, 'carla123', '23655896500', 47895632658, 'email@email.com', '2022-04-04 15:46:26'),
	(12, 2, 2, 2, 'carla', '23655896500', 47895632658, 'email@email.com', '2022-04-06 08:31:18'),
	(13, 1, 2, 2, 'carla', '589.698.659', 47895632658, 'maria@email.com', '2022-04-06 08:49:53'),
	(14, 7, 2, 2, 'carla', '23655896500', 0, 'email@email.com', '2022-04-06 13:15:41'),
	(16, 2, 2, 2, 'teste', '589.698.659', 0, 'carls@gmail.com', '2022-04-06 15:10:28');
/*!40000 ALTER TABLE `cadastrocliente` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.cadastrocondominio
CREATE TABLE IF NOT EXISTS `cadastrocondominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCond` varchar(50) NOT NULL DEFAULT '',
  `qtdeBlocos` varchar(50) NOT NULL DEFAULT '',
  `nomeSindico` varchar(50) NOT NULL DEFAULT '',
  `logradouro` varchar(50) NOT NULL DEFAULT '',
  `numero` varchar(50) NOT NULL DEFAULT '',
  `bairro` varchar(50) NOT NULL DEFAULT '',
  `cidade` varchar(50) NOT NULL DEFAULT '',
  `estado` varchar(50) NOT NULL DEFAULT '',
  `cep` int(11) NOT NULL DEFAULT 0,
  `fromAdministradora` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chAdm` (`fromAdministradora`),
  CONSTRAINT `chAdm` FOREIGN KEY (`fromAdministradora`) REFERENCES `administradoras` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.cadastrocondominio: ~12 rows (aproximadamente)
DELETE FROM `cadastrocondominio`;
/*!40000 ALTER TABLE `cadastrocondominio` DISABLE KEYS */;
INSERT INTO `cadastrocondominio` (`id`, `nomeCond`, `qtdeBlocos`, `nomeSindico`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `cep`, `fromAdministradora`) VALUES
	(1, 'Condominio Anna', '3', 'Camila', 'Rua das Rosas', '1523', 'Bairro Alegre', 'Rio de Janeiro', 'RJ', 123456, 1),
	(2, 'Condominio Village', '5', 'Dante Monteiro', 'Rua Royal', '5698', 'Bairro Veloz', 'São Paulo', 'SP', 256987, 3),
	(3, 'Condominio Bella Vista', '9', 'Martha Santana', 'Rua Santos', '236', 'Bom Retiro', 'Mochino', 'PA', 369520, 1),
	(6, 'Condominio Catarina', '15', 'Amanda Santana', 'Rua das bees', '569', 'Bairro Limp', 'Indaial', 'SC', 23659840, 1),
	(7, 'Condominio Ornua', '5', 'Artur Medeiras', 'Oscar Mews', '097', 'Nova Cruz', 'Pawleys Island', 'SC', 369520, 1),
	(8, 'Condominio Corau', '4', 'Luísa Castro', 'Rolfson Fords', '2301', 'Parque Imperial', 'Third Lake', 'MG', 369520, 2),
	(10, 'Condominio Ollar', '6', 'Elisa Lopes', 'Marquise Lane', '19', 'Igrejinha', 'Winterport', 'RJ', 369520, 2),
	(30, 'testeeeeeee', '2', 'joao', 'Rua das flores', '123', 'teste', 'Blumenau', 'PR', 25635948, 1),
	(31, 'teste', '4', 'Maria Joanna', 'das Flores', '123', 'Bairro Feliz', 'blumenau', 'PB', 25635948, 1),
	(35, 'Condominio Tarumã 2', '4', 'Carla Soares', 'das Flores', '369', 'teste', 'blumenau', 'MS', 25635948, 1),
	(36, 'Edificio Espacial X', '3', 'Maria Joanna', 'das Nuvens', '678', 'Bairro Turbo', 'Cachoeirinha', 'MT', 25635948, 1),
	(37, 'Edificio Espacial AE', '4', 'Joanna da Silva', 'das Flores', '123', 'teste', 'blumenau', 'PB', 25635948, 1);
/*!40000 ALTER TABLE `cadastrocondominio` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.cadastroconselho
CREATE TABLE IF NOT EXISTS `cadastroconselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(50) NOT NULL DEFAULT '',
  `telefone` bigint(20) NOT NULL DEFAULT 0,
  `funcao` enum('subsindico','conselheiro') NOT NULL,
  `fromCondominio` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `chConselho` (`fromCondominio`) USING BTREE,
  CONSTRAINT `chConselho` FOREIGN KEY (`fromCondominio`) REFERENCES `cadastroconselho` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.cadastroconselho: ~7 rows (aproximadamente)
DELETE FROM `cadastroconselho`;
/*!40000 ALTER TABLE `cadastroconselho` DISABLE KEYS */;
INSERT INTO `cadastroconselho` (`id`, `nome`, `cpf`, `telefone`, `funcao`, `fromCondominio`) VALUES
	(1, 'Joanna Silva', '12354968745', 47987563216, 'subsindico', 1),
	(2, 'Anna Silveira', '23605781960', 47987563216, 'conselheiro', 2),
	(3, 'testee 2389', '589.698.659.33', 0, 'conselheiro', 1),
	(4, 'testee23669', '589.698.659.33', 47895632658, 'subsindico', 2),
	(5, 'teste 2', '256.369.158.-05', 0, 'subsindico', 2),
	(9, 'testee', '589.698.659.33', 0, 'subsindico', 3),
	(10, 'testeeee', '589.698.659.33', 0, 'conselheiro', 3);
/*!40000 ALTER TABLE `cadastroconselho` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.cadastropets
CREATE TABLE IF NOT EXISTS `cadastropets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomePet` varchar(50) DEFAULT '0',
  `tipo` enum('cachorro','gato') DEFAULT NULL,
  `fromCliente` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.cadastropets: ~0 rows (aproximadamente)
DELETE FROM `cadastropets`;
/*!40000 ALTER TABLE `cadastropets` DISABLE KEYS */;
INSERT INTO `cadastropets` (`id`, `nomePet`, `tipo`, `fromCliente`) VALUES
	(1, 'anna', 'cachorro', 1);
/*!40000 ALTER TABLE `cadastropets` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.cadastrounidade
CREATE TABLE IF NOT EXISTS `cadastrounidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromCondominio` int(11) NOT NULL DEFAULT 0,
  `fromBloco` int(11) NOT NULL DEFAULT 0,
  `numeroUnidade` varchar(50) NOT NULL DEFAULT '0',
  `metragemUnidade` float NOT NULL DEFAULT 0,
  `qntdGaragem` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_cadastrounidade_cadastrocondominio` (`fromCondominio`),
  KEY `FK_cadastrounidade_cadastrobloco` (`fromBloco`),
  CONSTRAINT `FK_cadastrounidade_cadastrobloco` FOREIGN KEY (`fromBloco`) REFERENCES `cadastrobloco` (`id`),
  CONSTRAINT `FK_cadastrounidade_cadastrocondominio` FOREIGN KEY (`fromCondominio`) REFERENCES `cadastrocondominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.cadastrounidade: ~4 rows (aproximadamente)
DELETE FROM `cadastrounidade`;
/*!40000 ALTER TABLE `cadastrounidade` DISABLE KEYS */;
INSERT INTO `cadastrounidade` (`id`, `fromCondominio`, `fromBloco`, `numeroUnidade`, `metragemUnidade`, `qntdGaragem`) VALUES
	(1, 2, 3, '569', 69, 1),
	(2, 1, 2, '123', 53, 2),
	(3, 3, 1, '547', 23, 3),
	(4, 2, 2, '1234', 3, 3),
	(7, 10, 3, '121', 369, 2),
	(8, 7, 5, '56', 6, 33);
/*!40000 ALTER TABLE `cadastrounidade` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.cadastrousuarios
CREATE TABLE IF NOT EXISTS `cadastrousuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `senha` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.cadastrousuarios: ~1 rows (aproximadamente)
DELETE FROM `cadastrousuarios`;
/*!40000 ALTER TABLE `cadastrousuarios` DISABLE KEYS */;
INSERT INTO `cadastrousuarios` (`id`, `nome`, `usuario`, `senha`) VALUES
	(2, 'Anna', 'anna', '698d51a19d8a121ce581499d7b701668');
/*!40000 ALTER TABLE `cadastrousuarios` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.listaconvidados
CREATE TABLE IF NOT EXISTS `listaconvidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '0',
  `cpf` int(11) NOT NULL DEFAULT 0,
  `celular` bigint(20) NOT NULL DEFAULT 0,
  `fromReservaSalão` int(11) NOT NULL DEFAULT 0,
  `fromUnidade` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chUnidadeFesta` (`fromUnidade`),
  KEY `chReserva` (`fromReservaSalão`),
  CONSTRAINT `chReserva` FOREIGN KEY (`fromReservaSalão`) REFERENCES `reservasalaofesta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `chUnidadeFesta` FOREIGN KEY (`fromUnidade`) REFERENCES `cadastrounidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.listaconvidados: ~8 rows (aproximadamente)
DELETE FROM `listaconvidados`;
/*!40000 ALTER TABLE `listaconvidados` DISABLE KEYS */;
INSERT INTO `listaconvidados` (`id`, `nome`, `cpf`, `celular`, `fromReservaSalão`, `fromUnidade`, `dataCadastro`) VALUES
	(1, 'Dante Mendes', 2147483647, 47985632145, 1, 3, '2022-03-30 15:11:15'),
	(2, 'Anna Mendes', 1254563256, 47985678905, 2, 2, '2022-03-30 16:13:00'),
	(3, 'Carlos André', 1256205899, 47985601254, 1, 1, '2022-03-30 16:13:05'),
	(4, 'Ivone Martello', 2147483647, 78563232145, 2, 3, '2022-03-30 16:12:43'),
	(8, 'Reinaldo Azenha', 2147483647, 78563232145, 1, 1, '2022-03-30 16:12:57'),
	(9, 'Ellie Fartaria ', 2147483647, 78563232145, 2, 3, '2022-03-30 16:12:50'),
	(10, 'Amanda Vergueiro ', 2147483647, 78563232145, 3, 2, '2022-03-30 16:13:02'),
	(11, 'Jonas Seixas ', 2147483647, 78563232145, 1, 3, '2022-03-30 15:11:15');
/*!40000 ALTER TABLE `listaconvidados` ENABLE KEYS */;

-- Copiando estrutura para tabela projetocondominio.reservasalaofesta
CREATE TABLE IF NOT EXISTS `reservasalaofesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeEvento` varchar(255) NOT NULL DEFAULT '',
  `fromUnidade` int(11) NOT NULL,
  `dataEvento` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chUnidades` (`fromUnidade`),
  CONSTRAINT `chUnidades` FOREIGN KEY (`fromUnidade`) REFERENCES `cadastrounidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela projetocondominio.reservasalaofesta: ~3 rows (aproximadamente)
DELETE FROM `reservasalaofesta`;
/*!40000 ALTER TABLE `reservasalaofesta` DISABLE KEYS */;
INSERT INTO `reservasalaofesta` (`id`, `nomeEvento`, `fromUnidade`, `dataEvento`, `dataCadastro`) VALUES
	(1, 'Festa Casamento', 1, '0000-00-00 00:00:00', '2022-03-30 13:37:40'),
	(2, 'Formatura', 3, '0000-00-00 00:00:00', '2022-03-30 13:38:23'),
	(3, 'Festa Aniversário', 2, '0000-00-00 00:00:00', '2022-03-30 13:39:34');
/*!40000 ALTER TABLE `reservasalaofesta` ENABLE KEYS */;

-- Copiando estrutura para view projetocondominio.v_conselho
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `v_conselho` 
) ENGINE=MyISAM;

-- Copiando estrutura para view projetocondominio.v_conselho
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `v_conselho`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `v_conselho` AS SELECT cond.id, cond.nomeCond, cond.nomeSindico,

(SELECT GROUP_CONCAT(' ', nome) FROM cadastroconselho conselho WHERE conselho.from_condominio=cond.id) AS conselho

FROM cadastrocondominio cond ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
