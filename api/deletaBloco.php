<?
require "../uteis.php";
// require "../classes/cadBlocos.Class.php";

$blocos = new CadBlocos();

if($blocos -> deletaBloco($_POST['id'])){
    $totalRegistros = count($_SESSION['blocos']);

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Seu registro foi deletado com sucesso.",
    );

    echo json_encode($result);

} else{

    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado.",
    );

    echo json_encode($result);
}

?>