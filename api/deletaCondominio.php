<?
require "../uteis.php";

$condominio = new CadCondominio();
$result2 = $condominio->deletaCondominio($_POST['id']);
if($result2){
    
    $totalRegistros = $condominio->getCondominio()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, seu registro foi deletado"
    );

    echo json_encode($result);
} else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado"
    );

    echo json_encode($result);    
}
?>