<?
require "../uteis.php";
// require "../classes/cadBlocos.Class.php";

$blocos = new CadBlocos();

if($blocos -> setBlocos($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Registro inserido com sucesso.",
    );

    echo json_encode($result);

} else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser inserido.",
    );

    echo json_encode($result);
};

?>