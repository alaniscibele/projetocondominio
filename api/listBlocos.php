<?
require "../uteis.php";


$blocos = new cadBlocos();
$dadosBlocos = $blocos->getBlocoFromCond($_REQUEST['id']);

if(!empty($dadosBlocos)){
    $result = array(
        "status" => 'success',
        "resultSet" => $dadosBlocos['resultSet']
    );

} else{
    $result = array(
        "status" => 'danger',
        "msg" => "O cadastro não pode ser inserido"
    );

    
}

echo json_encode($result); 

?>