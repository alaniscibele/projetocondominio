<?

	function __autoload($class_name) {

		global $full_path;
		
		$upper_class_name = $full_path.class_dir.ds.strtolower($class_name).'.class.php';
		$class_name = $full_path.class_dir.ds.$class_name.'.class.php';
		
		if(file_exists($class_name)){
			require_once $class_name;
		} elseif(file_exists($upper_class_name)){
			require_once $upper_class_name;
		}

	}
	function natksort($array)	{

		$original_keys_arr = array();
		$original_values_arr = array();
		$clean_keys_arr = array();
	
		$i = 0;
		foreach ($array as $key => $value) {
			$original_keys_arr[$i] = $key;
			$original_values_arr[$i] = $value;
			$clean_keys_arr[$i] = strtr($key, "ÄÖÜäöüÉÈÀËëéèàç", "AOUaouEEAEeeeac");
			$i++;
		}
	
		natcasesort($clean_keys_arr);
	
		$result_arr = array();
	
		foreach ($clean_keys_arr AS $key => $value)
		{
			$original_key = $original_keys_arr[$key];
			$original_value = $original_values_arr[$key];
			$result_arr[$original_key] = $original_value;
		}
	
		return $result_arr;
	} 
	function getBrowser()	{

		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		$bname = 'Unknown';
		$platform = 'Unknown';
		$version= "";
	
		//First get the platform?
		if (preg_match('/linux/i', $u_agent)) {
			$platform = 'linux';
		}
		elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
			$platform = 'mac';
		}
		elseif (preg_match('/windows|win32/i', $u_agent)) {
			$platform = 'windows';
		}
	   
		// Next get the name of the useragent yes seperately and for good reason
		if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		}
		elseif(preg_match('/Firefox/i',$u_agent))
		{
			$bname = 'Mozilla Firefox';
			$ub = "Firefox";
		}
		elseif(preg_match('/Chrome/i',$u_agent))
		{
			$bname = 'Google Chrome';
			$ub = "Chrome";
		}
		elseif(preg_match('/Safari/i',$u_agent))
		{
			$bname = 'Apple Safari';
			$ub = "Safari";
		}
		elseif(preg_match('/Opera/i',$u_agent))
		{
			$bname = 'Opera';
			$ub = "Opera";
		}
		elseif(preg_match('/Netscape/i',$u_agent))
		{
			$bname = 'Netscape';
			$ub = "Netscape";
		}
	   
		// finally get the correct version number
		$known = array('Version', $ub, 'other');
		$pattern = '#(?<browser>' . join('|', $known) .
		')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
		if (!preg_match_all($pattern, $u_agent, $matches)) {
			// we have no matching number just continue
		}
	   
		// see how many we have
		$i = count($matches['browser']);
		if ($i != 1) {
			//we will have two since we are not using 'other' argument yet
			//see if version is before or after the name
			if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
				$version= $matches['version'][0];
			}
			else {
				$version= $matches['version'][1];
			}
		}
		else {
			$version= $matches['version'][0];
		}
	   
		// check if we have a number
		if ($version==null || $version=="") {$version="?";}
	   
		return array(
			'userAgent' => $u_agent,
			'name'      => $bname,
			'version'   => $version,
			'platform'  => $platform,
			'pattern'    => $pattern
		);
	} 

	/* --- */
	if (!function_exists('class_alias')) {
		function class_alias($original, $alias) {
			eval('abstract class ' . $alias . ' extends ' . $original . ' {}');
		}
	}
	
	$units = explode(' ', 'B KB MB GB TB PB');
	function foldersize($path) {
		$total_size = 0;
		$files = scandir($path);
		$cleanPath = rtrim($path, '/'). '/';
	
		foreach($files as $t) {
			if ($t<>"." && $t<>"..") {
				$currentFile = $cleanPath . $t;
				if (is_dir($currentFile)) {
					$size = foldersize($currentFile);
					$total_size += $size;
				}
				else {
					$size = filesize($currentFile);
					$total_size += $size;
				}
			}   
		}
	
		return $total_size;
	}
	function format_size($size) {
		global $units;
	
		$mod = 1024;
	
		for ($i = 0; $size > $mod; $i++) {
			$size /= $mod;
		}
	
		$endIndex = strpos($size, ".")+3;
	
		return substr( $size, 0, $endIndex).' '.$units[$i];
	}
	
	function modformat($t) {
		return number_format($t,2,',','.');
	}
	function moddesformat($t) {
		$t = str_replace('.', '', $t);
		$t = str_replace(',', '.', $t);
		return $t;

	}
	function numformat($str){

		$str = str_replace('.','',$str);
		$str = str_replace(',','.',$str);
		$str = $str == 0 ? 0 : $str;
		return $str;
	}
	/* --- */
	function  active($page,$str) {
		if(is_array($str)) {
			if(in_array($page,$str)) {
				return 'class="active"';
			}
		} else {
			if($page == $str) { 
				return 'class="active"';
			}
		}
	}
	function utime () {
		$time = explode(' ', microtime());
		$usec = (double)$time[0];
		$sec = (double)$time[1];
		return $sec + $usec;
	} 
	//$start = utime();
	function randr($j = 8){
		$string = "";
		for($i=0;$i < $j;$i++){
			srand((double)microtime()*1234567);
			$x = mt_rand(1,2);
			switch($x){
				case 0: $string.= chr(mt_rand(97,122)); break; // letras minúsculas
				case 1: 
					$r = mt_rand(65,90);
					$r = ($r==73)?($r+1):($r);// letra i
					$r = ($r==79)?($r+1):($r);// letra o
					$string.= chr($r); 
				break;
				case 2: $string.= chr(mt_rand(49,57)); break;
			}
		}
		return strtoupper($string); //to uppercase
	}
	function generateCouponCode($length = 8) {
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$ret = '';
		for($i = 0; $i < $length; ++$i) {
			$random = str_shuffle($chars);
			$ret .= $random[0];
		}
		return $ret;
	}
	
	function full_copy( $source, $target ) {
		if ( is_dir( $source ) ) {
			@mkdir( $target,0777,true );
			$d = dir( $source );
			while ( FALSE !== ( $entry = $d->read() ) ) {
				if ( $entry == '.' || $entry == '..' ) {
					continue;
				}
				$Entry = $source . '/' . $entry; 
				if ( is_dir( $Entry ) ) {
					full_copy( $Entry, $target . '/' . $entry );
					continue;
				}
				copy( $Entry, $target . '/' . $entry );
			}
	
			$d->close();
		}else {
			copy( $source, $target );
		}
	}
	



	function senha ($tipo='lnln') {
		$tipo = str_split($tipo);
		$l = str_split('abcdefghijklmnopqrstuvxwyz+');
		$lof = sizeof($l);
		$n = str_split('0123456789');
		$nof = sizeof($n);
		$senha=NULL;
		$sof=sizeof($tipo);
		for($i=0;$i<$sof;$i++) {
			if ($tipo[$i] == "l") {
				$senha.= $l[rand(0,$lof)];
			} elseif($tipo[$i] == "n") {
				$senha.= $n[rand(0,$nof)];
			}
		}
		return $senha;
	}
	function login($tamanho) {
		$conteudo = "abcdefghijklmnopqrstuvwxyz";
		for($i=0;$i<$tamanho;$i++) {
			$string .= $conteudo{rand(0,25)};
		}
		return $string;
	}
	function slider ($swf='swf/slides',$w='443',$h='326',$p,$serial) {
		
		$f = new ooflash($swf,$w,$h);
		$f->params('salign','TL');
		$strvars = 'n='.count($serial).'&amp;p='.$p;
		$i=1;
		foreach($serial as $key => $value){
			$strvars .= '&amp;f'.$i.'='.$key;
			$strvars .= '&amp;d'.$i.'='.$value;
			$i++;
		}
		$strvars .= '&amp;cor=990000';
		$f->params('flashvars',$strvars);
		$f->flwrite();
	}
	function aviso($class,$aviso){
		//echo $class;exit;
		$_SESSION['aviso']=$class.'|'.$aviso;
	}
	function jsalert($string = '',$mode = 'back',$url = '') {
		// echo $url; exit;
		echo "<script type=\"text/javascript\">";
		if(!empty($string)) {
			echo "alert('".$string."');";
		} else {
			$string = NULL;
		}
		if($mode == 'header') {
		 	echo "
			window.location='".$url."';";
			$destino = "<a href='".$url."'>Clique aqui</a>";
		}
		echo "</script>";
	}
	function apprise($msg,$tipo='ok',$url=''){
		$dd = "<script type=\"text/javascript\">$(function(){";
		switch ($tipo) {
			case 'no':
				$tp = 'no';
				break;
			case 'atencao':
				$tp = 'atencao';
				break;
			default:
				$tp = 'ok';
				break;
		}
		$dd .= "apprise('<img src=".$url_site."+'images/".$tp.".png\"/><br><span class=\"resposta-".$tp."\">".$msg."</span>', {'animate':true});";
		if(!empty($url)){
			$dd .= "location.url = '".$url."'";
		}
		$dd .= "})</script>";

		return $dd;
	}
	
	function legivel($var,$width = '250',$height = '400') {
		echo "<pre>";
		if(is_array($var)) {
			print_r($var);
		} else {
			print($var);
		}
		echo "</pre>";
	}
	function dump($var) {
		echo "<pre>";
		var_dump($var);
		echo "</pre>";
	}
	function text_to_links ($data='') {
	  if(empty($data)) { return $data; }
	  $lines = explode("\n", $data);

	  if(strpos($data,"<html"))
			return $data;
	  while ( list ($key,$line) = each ($lines)) {
		$line = preg_replace("/([ \t]|^)www\./"," http://www.",$line);
		$line = preg_replace("/(http:\/\/[^ )\r\n]+)/i","<a href=\"\\1\" target=\"_blank\">\\1</a>",$line);
		$line = preg_replace("/(https:\/\/[^ )\r\n]+)/i","<a href=\"\\1\" target=\"_blank\">\\1</a>",$line);
		$line = preg_replace("/(ftp:\/\/[^ )\\r\\n]+)/i","<a href=\"\\1\" target=\"_blank\">\\1</a>",$line);
		$line = preg_replace("/([-a-z0-9_]+(\.[_a-z0-9-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)+))/i","<a href=\"mailto:\\1\">\\1</a>",$line);
		$line = stripslashes($line);
		$newText .= $line."\n";
	  }
	  return $newText;
	}
	function _md5($str,$num=1){
		$md5='';
		for($i=0;$i < $num;$i++){
			$str = md5($str);
			$md5=$str;
		}
		return $md5;
	}
	function url($url){

		if(URL_FIX){
			$url = preg_replace('/^pagina\/([-_a-z0-9]+)\/?$/','index.php?page=$1',$url);
			$url = preg_replace('/^pagina\/([-_a-z0-9]+)\/parte-([0-9]+)\/?$/','index.php?page=$1&pag=$2',$url);
			$url = preg_replace('/^pagina\/([-_a-z0-9]+)\/([0-9]+)\/?([-_a-z0-9]+)\/?parte-([0-9]+)?\/?$/','index.php?page=$1&id=$2&permalink=$3&pag=$4',$url);
			$url = preg_replace('/^pagina\/([-_a-z0-9]+)\/galeria-parte-([0-9]+)\/?$/','index.php?page=$1&gpag=$2',$url);
			$url = preg_replace('/^pagina\/([-_a-z0-9]+)\/([0-9]+)?\/?([-_a-z0-9]+)\/?galeria-parte-([0-9]+)?\/?$/','index.php?page=$1&id=$2&permalink=$3&gpag=$4',$url);
			$url = preg_replace('/^pagina\/([-_a-z0-9]+)\/([0-9]+)\/?([-_a-z0-9]+)?\/?$/','index.php?page=$1&id=$2&permalink=$3',$url);
			$url = preg_replace('/^pagina\/([-_a-z0-9]+)\/([-_a-z0-9]+)\/?$/','index.php?page=$1&permalink=$2',$url);
		}

		return $url;
	}
	function thumbs($img){
		if(URL_FIX){
			$img = preg_replace('/^thumbs\/([0-9]+)x([0-9]+)\/([a-zA-Z0-9]+)x([0-9])\/(.*)$/','t3.php?image=$5&w=$1&h=$2&color=$3&el=$4',$img);
			$img = preg_replace('/^crop\/([0-9]+)x([0-9]+)\/([.0-9]+)x([.0-9]+)\/(.*)$/','c1.php?image=$5&width=$1&height=$2&cropratio=$3:$4',$img);
			$img = preg_replace('/^round\/([0-9]+)x([0-9]+)\/([0-9]+)\/(.*)$/','b1.php?src=$4&w=$1&h=$2&fltr[]=ric|$3|$3&zc=1&f=png',$img);
			$img = preg_replace('/^images\/([0-9]{1,3})_([a-zA-Z0-9]{6})\.png$/','px.php?hex=$2&o=$1',$img);
		}
		return $img;
	}
	
	function formatUrl ($str){
		if(preg_match('/^http:\/\//',$str)){
			return $str;
		} else {
			return $str;
		}
	}
	function u($str) {
		global $url_site;
		$a = array('[',']','');
		$b = array('%255B','%255D','&amp;');
		return $url_site.str_replace($a,$b,$str);
	}
	
	function incClass($str){
		$cls = explode(',',$str);
		foreach($cls as $class){
			$c = $full_path.include_dir.ds.$class.'.class.php';
			if(class_exists($class)){
				include($c);
			}
		}
	}
	
	function sqlFormat ($tratar,$vDimensions='640x480'){
		
		$vd = explode('x',$vDimensions);
		//$tratar = stripslashes($tratar);
		$er = '/^\[__([a-z]+)__\]/';
		/*if(gettype($tratar == 'string') {
			$tratar = utf8_encode($tratar);
		}*/
		if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $tratar)) {
			$tratar = date("d/m/Y", strtotime($tratar));
		}
		if(preg_match('/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/', $tratar)) {
			$tratar = date("H:i", strtotime($tratar));
		}
		
		if(preg_match($er,$tratar,$o)) {
			/*
			$o[0] == '[__slider__]';
			$o[1] == 'slider';
			*/
			$tratar = ereg_replace($er,'', $tratar);
			switch($o[1]) {
				case "files":
					$tratar = str_replace('[__files__]','',$tratar);
					$tratar = explode("|", $tratar);
				break;
				case "video":
					$tratar = videoFormat($tratar);
				break;
				case "text":
					$tratar = limpaInvertido($tratar);
				break;
				case "serial":
					$tratar = unserialize($tratar);
				break;
			}
		}
		return $tratar;
	
	}
	
	function videoFormat($url,$er,$w,$h,$vOldMode=true){
		
		global $vParams;
		
		if(preg_match('/[a-zA-Z0-9]+\.com\//',$url,$out)){
			$url = preg_replace($er, '', $url);
			//print_r($url);
			switch($out[0]){
				case "youtube.com/":
					preg_match('/(\?|&)v=([0-9a-zA-Z_-]+)(&|$)/si', $url, $m);
					// $m[2] <---- é o ID do vídeo
					$tratar = array();
					$tratar['vid'] = ($url != '')?($m[2]):('');
					$tratar['url'] = ($url != '')?('http://www.youtube.com/watch?v='.$m[2]):('');
					$tratar['img'] = ($url != '')?('http://i'.rand(1,4).'.ytimg.com/vi/'.$m[2].'/default.jpg'):('t2.php?image=images/semimagem.jpg&amp;w=120');
					$tratar['hqimg'] = ($url != '')?('http://i'.rand(1,4).'.ytimg.com/vi/'.$m[2].'/hqdefault.jpg'):('t2.php?image=images/semimagem.jpg&amp;w=120');
					if($vOldMode){
						$tratar['embed'] = ($url == '')?('<p>Vídeo indisponível</p>'):('<div><object width="'.$w.'" height="'.$h.'">
						<param name="movie" value="http://www.youtube.com/v/'.$m[2].'&hl=pt-br&fs=1" />
						<param name="allowFullScreen" value="true" />
						<param name="allowscriptaccess" value="always" />
						<embed src="http://www.youtube.com/v/'.$m[2].'&hl=pt-br&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="'.$w.'" height="'.$h.'"></embed>
						</object></div>');
					} else {
						$tratar['embed'] = ($url == '')?('<p>Vídeo indisponível</p>'):('<iframe width="'.$w.'" height="'.$h.'" src="http://www.youtube.com/embed/'.$m[2].$vParams.'" frameborder="0" allowfullscreen></iframe>');
					}
					
				break;
				case "vimeo.com/":
					$spl = explode('|',$url);
					preg_match('/vimeo\.com\/([0-9]+)/', $spl[0], $m);
					// $m[1] <---- é o ID do vídeo
					$tratar = array();
					$tratar['url'] = ($url == '')?(''):($spl[0]);
					$tratar['img'] = ($url == '')?(''):($spl[1]);
					$tratar['embed'] = ($url == '')?('<p>Vídeo indisponível</p>'):('<div><iframe src="http://player.vimeo.com/video/'.$m[1].'" width="'.$w.'" height="'.$h.'" frameborder="0"></iframe></div>');
				break;
			}
			return $tratar;
		}
	}
	function codificacao($string) {
		//return mb_detect_encoding($string.'x', 'UTF-8, ISO-8859-1');
		return mb_detect_encoding($string.'x');
	
	}
	/*function utf8Fix($str){
		if(codificacao($str) == 'ASCII') {
			return $str;
		} else {
			return utf8_encode($str);
		}
	}*/
	function _substr ($string, $length_ini = 0, $length_end = 50,$is_en='(...)'){
		//$enc1 = codificacao($string);
		$valor = (codificacao($string) != 'UTF-8')?($string):(utf8_decode($string));
		$valor = strip_tags($valor);
		//$enc2 = codificacao($valor);
		$valor = html_entity_decode($valor);
		//$enc3 = codificacao($valor);
		$fim = $is_en;
		/*if($is_en != false){
			$fim = '';
		} else {
			$fim = '(...)';
		}*/

		$valor = (
			strlen($valor) > $length_end
		)?(
			substr($valor,$length_ini,$length_end).$fim /*.$enc1." ".$enc2." ".$enc3*/
		):(
			$valor
		); 
		$valor = htmlentities($valor);
		$valor = str_replace('&amp;ndash;','&ndash;',$valor);
		$valor = str_replace('&amp;ldquo;','&ldquo;',$valor);
		$valor = str_replace('&amp;rdquo;','&rdquo;',$valor);
		return $valor;
	}

	function _data ($date,$undo = false) {
		if ($undo == false) {
			$arrayData = explode('-',$date);
			return $arrayData[2].'/'.$arrayData[1].'/'.$arrayData[0];
		} else {
			$arrayData = explode('/',$date);
			return $arrayData[2].'-'.$arrayData[1].'-'.$arrayData[0];
		}
	}
	function _dataHora ($date,$undo = false) {
		if ($undo == false) {
			return date("d/m/Y H:i:s", strtotime($date));
		} else {
			return date("Y-m-d H:i:s", strtotime($date));
		}
	}
	
	function dateformat($date,$spl='/') {
		$nspl = ($spl=='/')?('-'):('/');
		$date = explode($spl, $date);
		$date = array_reverse($date);
		$date = implode($nspl,$date);
		return $date;
	}

	function dateTimeHiffen($date,$spl='/') {
		$nspl = ($spl=='/')?('-'):('/');
		$time = explode(' ', $date);
		$date = explode($spl, $time[0]);
		$date = array_reverse($date);
		$date = implode('-',$date).' '.$time[1];
		return $date;
	}

	function diffDate($inicio, $fim,$x='d') {
		switch($x){
			case "d":
				$x = 86400;
			break;
			case "m":
				$x = 1036800;
			break;
			case "a":
				$x = 31536000;
			break;
		}
		$dataInicio = strtotime($inicio);
		$dataFim    = strtotime($fim);
		//echo $dataFim;exit;
		$intervalo  = ($dataFim-$dataInicio);
		return $intervalo/$x;
	}
	function number_pad($number,$n) {
		return str_pad((int) $number,$n,"0",STR_PAD_LEFT);
	}
	function emailAnswer ($str){
		//$str = wordwrap($str, 70, "\n",false);
		$str = trim($str);
		$str = explode("\n",$str);
		$nstr = "\n\n\n";
		foreach($str as $fill){
			$nstr .= "//".$fill."\n";
		}
		return $nstr;
	}
	function timeStampDate($strtotime){
		return date('Y-m-d H:i:s', strtotime($strtotime));
	}
	function timeStampDateBr($date){
		$ex = explode(' ',$date);

		$data = array_reverse(explode('-',$ex[0]));
		$data = implode('/',$data);
		$hora = $ex[1];
		
		return $data.' '.$hora;
	}
	function mask($val, $mask){
		
		$maskared = '';
		$k = 0;
		$val = preg_replace('/[^0-9]/','',$val);
		for($i = 0; $i<=strlen($mask)-1; $i++) {
			if($mask[$i] == '#'){
				if(isset($val[$k]))
					$maskared .= $val[$k++];
			} else {
				if(isset($mask[$i]))
					$maskared .= $mask[$i];
			}
		}
		return $maskared;
	}
	function transformData($data){
		$data = explode('/',$data);
		return sprintf("%d de %s de %d",$data[0],meses($data[1]),$data[2]);
	}
	
	function mascara($campo, $formatado = true){

		$codigoLimpo = ereg_replace("[' '-./ t]",'',$campo);
		$tamanho = (strlen($codigoLimpo) -2);

		if ($tamanho != 9 && $tamanho != 12){
			return false;
		}
		
		if ($formatado){
			$mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##';
			$indice = -1;
			for ($i=0; $i < strlen($mascara); $i++) {
				if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
			}
			$retorno = $mascara;
		
		} else {
			$retorno = $codigoLimpo;
		}
		
		return $retorno;
	
	}	
	function validaEmail($email, $exp = "/^[+_a-z0-9-]+(\.[+_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)+$/"){
			/*echo $exp;
			exit;*/
        if (preg_match($exp, $email)){
            return true;
        } else {
            return false;
        }
    }
	function validaCPF($cpf) {
   		$cpf = preg_replace('/[^0-9]/','',$cpf);
		$soma = 0;
		
		if (strlen($cpf) <> 11)
			return false;
		
		// Verifica 1º digito      
		for ($i = 0; $i < 9; $i++) {         
			$soma += (($i+1) * $cpf[$i]);
		}
		
		$d1 = ($soma % 11);
		
		if ($d1 == 10) {
			$d1 = 0;
		}
		
		$soma = 0;
		
		// Verifica 2º digito
		for ($i = 9, $j = 0; $i > 0; $i--, $j++) {
			$soma += ($i * $cpf[$j]);
		}
		
		$d2 = ($soma % 11);
		
		if ($d2 == 10) {
			$d2 = 0;
		}      
		
		if ($d1 == $cpf[9] && $d2 == $cpf[10]) {
			return true;
		} else {
			return false;
		}
	}
   
   
   function mail_smtp($to,$subject,$message,$headers=''){

		$mail = new PHPMailer();
		$mail->IsHTML(true);
		$mail->IsSMTP();           // set mailer to use SMTP
		$mail->Host = host_email;  // specify main and backup server
		$mail->SetLanguage("br", $full_path.class_dir.ds."language/");
		$mail->SMTPAuth = $auth;     // turn on SMTP authentication
		$mail->Username = login_email;  // SMTP username
		$mail->Password = senha_email; // SMTP password
		$mail->Sender = $this->sender;
		$mail->From = (empty($this->from))?(envio_email):($this->from);
		$mail->FromName = $this->fromname;

		foreach($this->destino as $destino){
			$mail->AddAddress($destino);
		}

		$mail->Subject = $this->assunto;
		$mail->Body = $this->mensagem;
		$mail->Send();
		$mail->ClearAddresses();
		$this->clearMessage();
		if(empty($mail->ErrorInfo)) {
			if($this->redir != false) {
				if(!$this->returnValue){
					jsalert($this->jsmensagem,'header',$this->redir);
				} else {
					$this->status = 'sucesso';
					return true;
				}
			}
		} else {
			echo $mail->ErrorInfo;
			exit;
		}
	}
   
   
   // VERFICA CNPJ
   function validaCNPJ($cnpj) {
   		$cnpj = preg_replace('/[^0-9]/','',$cnpj);
		if (strlen($cnpj) <> 14)
			return false;

		$soma = 0;

		$soma += ($cnpj[0] * 5);
		$soma += ($cnpj[1] * 4);
		$soma += ($cnpj[2] * 3);
		$soma += ($cnpj[3] * 2);
		$soma += ($cnpj[4] * 9);
		$soma += ($cnpj[5] * 8);
		$soma += ($cnpj[6] * 7);
		$soma += ($cnpj[7] * 6);
		$soma += ($cnpj[8] * 5);
		$soma += ($cnpj[9] * 4);
		$soma += ($cnpj[10] * 3);
		$soma += ($cnpj[11] * 2);

		$d1 = $soma % 11;
		$d1 = $d1 < 2 ? 0 : 11 - $d1;

		$soma = 0;
		$soma += ($cnpj[0] * 6);
		$soma += ($cnpj[1] * 5);
		$soma += ($cnpj[2] * 4);
		$soma += ($cnpj[3] * 3);
		$soma += ($cnpj[4] * 2);
		$soma += ($cnpj[5] * 9);
		$soma += ($cnpj[6] * 8);
		$soma += ($cnpj[7] * 7);
		$soma += ($cnpj[8] * 6);
		$soma += ($cnpj[9] * 5);
		$soma += ($cnpj[10] * 4);
		$soma += ($cnpj[11] * 3);
		$soma += ($cnpj[12] * 2);
		
		
		$d2 = $soma % 11;
		$d2 = $d2 < 2 ? 0 : 11 - $d2;
		
		if ($cnpj[12] == $d1 && $cnpj[13] == $d2) {
			return true;
		} else {
			return false;
		}
   } 
	function QuickGetPics ($strDiretorio, $qtd = 1) {

		$strDiretorioAbrir = @opendir($strDiretorio);
		$i = 1;
		$fotos = array();

		while ($strArquivos = @readdir($strDiretorioAbrir)) {
			if ($strArquivos != "." && $strArquivos != ".."  && $strArquivos != "Thumbs.db" && $strArquivos != ".gitkeep" && $strArquivos != "desktop.ini") {
				$fotos[] = $strDiretorio."/".rawurlencode($strArquivos);
				if($i == $qtd) { 
					break;
				}
				$i++;
			}
		}
		natcasesort($fotos);
		sort($fotos);
		return $fotos;
	}
	
	function getPics ($strDiretorio, $qtd = 1,$rand=false) {

		$strDiretorioAbrir = @opendir($strDiretorio);
		//$i = 1;
		$fotos = array();

		while ($strArquivos = @readdir($strDiretorioAbrir)) {
			if ($strArquivos != "." && $strArquivos != ".."  && $strArquivos != "Thumbs.db" && $strArquivos != ".gitkeep" && $strArquivos != "desktop.ini" && !is_dir($strDiretorio.$strArquivos)) {
				$fotos[] = $strDiretorio."/".rawurlencode($strArquivos);
			}
		}
		natcasesort($fotos);
		sort($fotos);
		
		if($rand){shuffle($fotos);}
		
		if($qtd==0){
			return $fotos;
		} else {
			$fotos = array_chunk($fotos,$qtd);
			return $fotos[0];
		}
	}
	function onePic($str){
		$strDiretorioAbrir = @opendir($strDiretorio);
		$i = 1;
		while ($strArquivos = @readdir($strDiretorioAbrir)) {
			if ($strArquivos != "." && $strArquivos != ".."  && $strArquivos != "Thumbs.db" && $strArquivos != "desktop.ini") {
				$foto = $strArquivos;
				break;
			}
		}
		return $foto;
	}
	function imgName($img){
		return end(explode('/', $img));
	}
	function urlFormat ($file) {
		$file = str_replace(' ','%20',$file);
		return $file;
	}
	function html2rgb($color){
		if ($color[0] == '#')
			$color = substr($color, 1);
	
		if (strlen($color) == 6)
			list($r, $g, $b) = array($color[0].$color[1],
									 $color[2].$color[3],
									 $color[4].$color[5]);
		elseif (strlen($color) == 3)
			list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
		else
			return false;
	
		$r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
	
		return array($r, $g, $b);
	}
	function http($str){
		if($str==''){
			return '#';
		}
		if(!preg_match('/^http[s]?:\/\//',$str,$m)){
			return 'http://'.$str; 
		} else {
			return $str;
		}
	}
	function mailto($str){
		return 'mailto:'.$str;
	}
	function meses ($m) {
		switch($m) {
			case 1:
			return "Janeiro";
			break;
			case 2:
			return "Fevereiro";
			break;
			case 3:
			return "Março";
			break;
			case 4:
			return "Abril";
			break;
			case 5:
			return "Maio";
			break;
			case 6:
			return "Junho";
			break;
			case 7:
			return "Julho";
			break;
			case 8:
			return "Agosto";
			break;
			case 9:
			return "Setembro";
			break;
			case 10:
			return "Outubro";
			break;
			case 11:
			return "Novembro";
			break;
			case 12:
			return "Dezembro";
			break;
		}
	}
	function mes_sig ($s) {
		switch($s) {
			case 1:
			return "Jan";
			break;
			case 2:
			return "Fev";
			break;
			case 3:
			return "Mar";
			break;
			case 4:
			return "Abr";
			break;
			case 5:
			return "Mai";
			break;
			case 6:
			return "Jun";
			break;
			case 7:
			return "Jul";
			break;
			case 8:
			return "Ago";
			break;
			case 9:
			return "Set";
			break;
			case 10:
			return "Out";
			break;
			case 11:
			return "Nov";
			break;
			case 12:
			return "Dez";
			break;
		}
	}	
	function dias ($d) {
		switch($d) {
			case 0:
			return "Domingo";
			break;
			case 1:
			return "Segunda";
			break;
			case 2:
			return "Terça";
			break;
			case 3:
			return "Quarta";
			break;
			case 4:
			return "Quinta";
			break;
			case 5:
			return "Sexta";
			break;
			case 7:
			return "Sábado";
			break;

		}
	}
	function dias_sig ($d) {
		switch($d) {
			case 0:
			return "Dom";
			break;
			case 1:
			return "Seg";
			break;
			case 2:
			return "Ter";
			break;
			case 3:
			return "Qua";
			break;
			case 4:
			return "Qui";
			break;
			case 5:
			return "Sex";
			break;
			case 7:
			return "Sáb";
			break;

		}
	}
	function hoje($hj){
		$hj = dateformat($hj,'/');
		$hj = strtotime($hj);
		$hj = date('w',$hj);
		$hj = dias_sig($hj);
		return $hj;
	}
	function dia($di){
		$di = dateformat($di,'/');
		$di = strtotime($di);
		$di = date('d',$di);
		//$an = meses($an);
		return $di;	
	}
	function mes($ma){
		$ma = dateformat($ma,'/');
		$ma = strtotime($ma);
		$ma = date('n',$ma);
		$ma = meses($ma);
		return $ma;	
	}
	function ano($an){
		$an = dateformat($an,'/');
		$an = strtotime($an);
		$an = date('Y',$an);
		//$an = meses($an);
		return $an;	
	}	
	function filedisplay($ar, $contexto, $limit = '') {
		$i = 0;
		foreach($ar as $keys => $img){
			if(!empty($img)){
				$w = str_replace("[*keys*]", $keys, $contexto);
				$w = str_replace("[*file*]", $img, $w);
				echo $w;
				$i++;
				if(!empty($limit)) {
					if($i == $limit) {
						break;
					}
				}
			}
		}
	}

	function usuario($array,$sesName = 'usuario'){
		//legivel($_SESSION);
		if($_SESSION[$sesName]) {
			$_SESSION[$sesName] = (array)$_SESSION[$sesName];
			return $_SESSION[$sesName][$array];
		} else {
			return "";
		}
	}	
	function warning($w=''){
		$av = explode('|',$_SESSION[$w]);
		$_SESSION[$w] = '';
		if(!empty($av[1])){
			$av[1] = str_replace('*','&',$av[1]);
			return "<div class=\"aviso ".$av[0]."\">
				".$av[1]."
			</div>";
		}
	}
	function strip_tags_c($string, $allowed_tags = '') {   
		$allow_comments = (strpos($allowed_tags,'<!-->') !== false);
		if($allow_comments) {
			$string = str_replace(array('<!--','-->'), array('&lt;!--','--&gt;'), $string);
			$allowed_tags = str_replace('<!-->', '', $allowed_tags);
		}
		$string = strip_tags( $string, $allowed_tags );
		if( $allow_comments ) $string = str_replace(array('&lt;!--', '--&gt;'), array('<!--', '-->'), $string);
		return $string;
	}	
	function closeUnclosedTags($unclosedString){
	  // created by Adam Gundry, http://www.agbs.co.uk
	  preg_match_all("/<([^\/]\w*)>/", $closedString = $unclosedString, $tags);
	  for ($i=count($tags[1])-1;$i>=0;$i--){
		$tag = $tags[1][$i];
		if (substr_count($closedString, "</$tag>") < substr_count($closedString, "<$tag>")) $closedString .= "</$tag>";
	  }
	  return $closedString;
	} 
	function limpa($str) {
		$t = str_replace('\'','&#039;',$str);
		$t = str_replace('"','&quot;',$t);
		return $t;
	}		
	function limpaInvertido($str) {
		$t = str_replace('&#039;','\'',$str);
		$t = str_replace('&quot;','"',$t);
		return $t;
	}		
	function tagEntities($str) {
		$t = str_replace('<','&lt;',$str);
		$t = str_replace('>','&gt;',$t);
		return $t;
	}
	function antiSpam($p){
		foreach($p as $post){
			if(preg_match('/\[url=/',$post)){
				exit;
			}
		}
	}
	function antiinject($var,$quotes=ENT_NOQUOTES,$keeptags=false) {
		//ENT_QUOTES, ENT_NOQUOTES, ENT_COMPAT;
		
		
		if(!is_array($var)){
			$var = stripslashes($var);
			$var = html_entity_decode($var,$quotes,'utf-8');
			if(!$keeptags) {
				$var = strip_tags($var);
			}
			$var = trim($var);
			//$var = utf8_decode($var);
			/**/
			$var = htmlentities($var,$quotes);
			if($keeptags) {
				$var = str_replace('&lt;','<',$var);
				$var = str_replace('&gt;','>',$var);
			}
			/**/
			$var = addslashes($var);
		} else {
			foreach($var as $k=>$ar){
				$var[$k] = antiinject($ar);
			}
		}
		return $var;
	}
	function ai($str){
		return antiinject($str);
	}
	function getext($texto){
		$getext = explode(".", $texto);
		$ext = end($getext);
		return $ext;
	}
	function renomeia($updir ='', $texto = '') {
		$t = time().rand(10,99);
		$getext = explode(".", $texto);
		$getext = array_reverse($getext);
		$ext = strtolower(array_shift($getext));
		$nome = implode('-',$getext);
		$nome = strtolower2($nome);
		$nome = desacento($nome);
		$nome = noBlank($nome);

		$x = 1;
		while(file_exists($updir.$novonome)) {
			$novonome = $nome."-".$x.".".$ext;
			$x++;
		}
		$nome = $novonome;
		if(empty($texto)) {
			return $t;
		} else {
			return $nome;
		}
	}
	function renomeia_simples($texto = '') {
		$t = time().rand(10,99);
		$nome = strtolower2($texto);
		$nome = desacento($nome);
		$nome = noBlank($nome);
		return $nome;
	}
	function strtolower2($string) {
	
		  $convert_to = array(
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
			"v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
			"ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
			"з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
			"ь", "э", "ю", "я"
		  );
		  $convert_from = array(
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
			"V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï",
			"Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
			"З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
			"Ь", "Э", "Ю", "Я"
		  );
		
		  return str_replace($convert_from, $convert_to, $string);
	} 
	function strtoupper2($string) {
	
		  $convert_from = array(
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
			"v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
			"ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
			"з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
			"ь", "э", "ю", "я"
		  );
		  $convert_to = array(
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
			"V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï",
			"Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
			"З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
			"Ь", "Э", "Ю", "Я"
		  );
		
		  return str_replace($convert_from, $convert_to, $string);
	} 
	function permalinkFormat($str=''){
		$str = html_entity_decode($str, ENT_QUOTES, 'utf-8');
		$str = strtolower2($str);
		$str = removeacento($str);
		$str = noBlank($str,'-');
		$str = stripacento($str);
		$str = preg_replace("/[-]{2,}/","-",$str);
		$str = substr($str,0,80);
		$str = trim($str,'-');
		return $str;
	}
	function curl($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_REFERER,
		$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
		curl_setopt($ch, CURLOPT_USERAGENT, "PHP_CURL");
		@curl_setopt($ch, CURLOPT_MUTE, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$string = curl_exec ($ch);
		curl_close ($ch);
		return $string;
	}
	function linkTwitter($str){
		$str = preg_replace("/@([a-zA-Z0-9_]+?)([[:space:]]|$)/is", '@<a href="http://www.twitter.com/\\1">\\1</a> ', $str);
		return $str;
	}
	function strip_only($str, $tags, $stripContent = false) {
		$content = '';
		if(!is_array($tags)) {
			$tags = (strpos($str, '>') !== false ? explode('>', str_replace('<', '', $tags)) : array($tags));
			if(end($tags) == '') array_pop($tags);
		}
		foreach($tags as $tag) {
			if ($stripContent)
				$content = '(.+</'.$tag.'[^>]*>|)';
			 	$str = preg_replace('#</?'.$tag.'[^>]*>'.$content.'#is', '', $str);
		}
		return $str;
	} 
	function stripacento($str){
		$str = preg_replace('/[^a-z0-9-_]/','',$str);
		$str = trim($str,'-');
		return $str;
	}
	function desacento($texto) {
		return removeacento($texto);
	}
	//áàãâéêíìóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ
	function removeacento($s) {
		$s = html_entity_decode($s, ENT_QUOTES, 'utf-8');
		$a = array(
		'á','à','ã','â','é','ê','í','ì','ó','ô','õ','ú','ü','ç','Á','À','Ã','Â','É','Ê','Í','Ó','Ô','Õ','Ú','Ü','Ç'
		);
		
		$b = array(
		'a','a','a','a','e','e','i','i','o','o','o','u','u','c','A','A','A','A','E','E','I','O','O','O','U','U','C'
		);
		$r = str_replace($a,$b,$s);
		return $r;
	}

	function noBlank($string,$tostr = '_') {
		$from = array(' ','%20','&','.','-');
		$to = $tostr;
		$m = str_replace($from,$to, $string);
		return $m;
	}

	function checkext ($file, $ext) {
		$nome = explode(".",strtolower($file));
		$exts = explode(',',strtolower($ext));
		$count = count($nome);
		if(in_array($nome[$count-1],$exts)) { 
			return true;
		} else {
			return false;
		}
	}

	function noext ($file) {
		$ex = explode('.', $file);
		$ext = end($ex);
		$file = preg_replace('/\.'.$ext.'$/','',$file);
		return $file;
	}
	
	function _check ($var, $var2,$im=',') {
		$var = (is_array($var))?($var):((array)$var);
		if(!is_array($var2)){
			$var2 = explode($im,$var2);
		}
		foreach($var as $v){
			if(in_array((string)$v,$var2)) {
				return " checked=\"checked\"";
			}
		}
	}

	function _select ($var, $var2,$im=',') {
		$var = (is_array($var))?($var):((array)$var);
		if(!is_array($var2)){
			$var2 = explode($im,$var2);
		}
		foreach($var as $v){
			if(in_array((string)$v,$var2)) {
				return " selected=\"selected\"";
			}
		}
	}
	
	function _disabled ($var, $var2) {
		$var = (is_array($var))?($var):((array)$var);
		if(!is_array($var2)){
			$var2 = explode(',',$var2);
		}
		foreach($var as $v){
			if(in_array((string)$v,$var2)) {
				return " disabled=\"disabled\"";
			}
		}
	}
	function _comp ($var, $var2,$im=',') {
		$var = (is_array($var))?($var):((array)$var);
		if(!is_array($var2)){
			$var2 = explode($im,$var2);
		}
		foreach($var as $v){
			if(in_array((string)$v,$var2)) {
				return $im;
			}
		}
	}
	function sql_error($sql,$det = '') {
		
		$r = "<div style=\"font-family:'Courier New', Courier, monospace;border:1px solid #ccc;background:#fafafa;padding:8px;margin:15px 0;font-size:.8em;\">";
		if(is_array($det)){
			$r .= "<div style=\"color:red;\"><b>Detalhes do erro</b>:<br />";
			$r .= "<pre>".implode("\n",$det)."</pre></div>";
		}
		$r .= "<b>Erro retornado do SQL:</b><br />";
		$r .= "<i>".mysql_error()."</i><br />";
		$r .= "<b>Comando SQL executado</b>:<br />";
		$r .= "<pre>".$sql."</pre><br />";
		$r .= "</div>";
		//----------------------------------------		
		error(mysql_errno(),mysql_error(),$_SERVER['REQUEST_URI'],$sql);
		return $r;
	}
	
	function error($numero,$texto,$arquivo,$linha){
		return false;
	}
	function erro_alert($alert) {
		echo "<div style=\"background:red;\">";
		echo $alert;
		echo "</div>";
		exit;
	}

	function troca_caracteres($texto,$cond=true){

		$entrada = array('&aacute;','&eacute;','&iacute;','&oacute;','&uacute;','&atilde;','&otilde;','&acirc;','&ecirc;','&icirc;','&ocirc;','&ucirc;','&ccedil;','&Aacute;','&Eacute;','&Iacute;','&Oacute;','&Uacute;','&Atilde;','&Otilde;','&Acirc;','&Ecirc;','&Icirc;','&Ocirc;','&Ucirc;','&Ccedil;','&agrave;','&Agrave;');
		
		$saida = array('á','é','í','ó','ú','ã','õ','â','ê','î','ô','û','ç','Á','É','Í','Ó','Ú','Ã','Õ','Â','Ê','Î','Ô','Û','Ç','à','À');
		if($cond){
			$result = str_replace($entrada, $saida, $texto);
		}else{
			$result = str_replace($saida, $entrada, $texto);
		}

		return $result;
	}
	
	function varrerSessao($sessao,$search){
		foreach($sessao as $s){
			if($s['id'] == $search){
				return true;
			}
		}
		return false;
	}	

	function leftPad($num){
		if($num < 10){
			$num = '0'.round($num,0);
		}else{
			$num = round($num,0);
		}

		return $num;
	}

	function retornaTempo($timeBD){

		$agora = date('Y-m-d H:i:s');

		$unix_data1 = $timeBD;
		$unix_data2 = strtotime($agora);

		$nHoras   = ($unix_data2 - $unix_data1) / 3600;
		$nMinutos = (($unix_data2 - $unix_data1) % 3600) / 60;
		$nSegundos = (($unix_data2 - $unix_data1) % 60);


		//arredondar
		$horas = floor($nHoras);
		$minutos = floor($nMinutos);
		$segundos = floor($nSegundos);

		//return $horas.' : '.$minutos.' : '.$segundos;

		if($minutos < 1 && $horas < 1){
			return "Há ".leftPad($segundos)."s";
		}

		if($minutos >= 1 && $minutos <= 59 && $horas == 0){
			return "Há ".leftPad($minutos)."m";
		}

		if($horas >= 1 && $horas < 24){
			return "Há ".leftPad($horas)."h".leftPad($minutos)."m";
		}

		if($horas >= 24){
			return date("d/m/y", $timeBD);
		}

	}

function procpalavras ($frase, $palavras, $resultado = 0) {
      foreach ( $palavras as $key => $value ) {
      $pos = strpos($frase, $value);
      if ($pos !== false) { $resultado = 1; break; }
      }
  return $resultado;
}	

//formatar valores tipos fone, cep, cpf, cnpj ou rg
function formatar($tipo = "", $string, $size = 10)
{
    $string = ereg_replace("[^0-9]", "", $string);
    
    switch ($tipo)
    {
        case 'fone':
            if($size === 10){
             $string = '(' . substr($tipo, 0, 2) . ') ' . substr($tipo, 2, 4) 
             . '-' . substr($tipo, 6);
         }else
         if($size === 11){
             $string = '(' . substr($tipo, 0, 2) . ') ' . substr($tipo, 2, 5) 
             . '-' . substr($tipo, 7);
         }
         break;
        case 'cep': 
            $string = substr($string, 0, 2).'.'.substr($string, 2, 3).'-'.substr($string, 5, 5);
         break;
        case 'cpf':
            $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) . 
                '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
         break;
        case 'cnpj':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . 
                '.' . substr($string, 5, 3) . '/' . 
                substr($string, 8, 4) . '-' . substr($string, 12, 2);
         break;
        case 'rg':
            $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) . 
                '.' . substr($string, 5, 3);
         break;
        default:
         $string = 'É ncessário definir um tipo(fone, cep, cpg, cnpj, rg)';
         break;
    }
    return $string;
}

//enviar email de cadastro de clientes

function enviaEmailCadastro($email,$senha,$nome){
	global $rootClass, $root, $urlUserLink;

	include($rootClass.'phpmailer.class.php');
	include($rootClass.'emailTemplate.class.php');
	
	$m = new emailTemplate($root.'client/email-template/email.php');
	//$m->dir = '../email-template/';
	$m->IsSMTP();
	$m->Subject = 'Dados de Acesso de Pedidos Online';
	$m->returnPath(email_padrao);
	$m->mailFrom('smtp@adboxdelivery.com.br');
	$m->emailTo($email);
	// $m->SMTPDebug = true;
	//----------------------------------------
		$m->arrayValues = array(
			'top' => 'Seja bem vindo(a) ao nosso site!',
			'data' => date('d/m/Y H:i:s'),
			'urlsite' => $urlUserLink,
			'msgtitle' => 'Olá '.$nome,
			'msg' => ' 
			<p>Olá '.$nome.', você está recebendo a senha para poder fazer seu próximo pedido pelo nosso site. É mais fácil e prático.</p>
			<p>Seu login é: <b>'.$email.'</b></p>
			<p>Sua senha é: <b>'.$senha.'</b></p>',
			'actionbutton' => $urlUserLink,
			'labelbutton' => 'FAZER PEDIDO'
		);
	//----------------------------------------
	//legivel($_POST);
	//exit;
	// $m->show();
	if($m->mail_send()){
		return 'ok';
	} else {
		return 'no';
	}
	
}

function geraCor() {
    $letters = '0123456789ABCDEF';
    $color = '#';
    for($i = 0; $i < 6; $i++) {
        $index = rand(0,15);
        $color .= $letters[$index];
    }
    return $color;
}

function recursiveArraySearch($obj, $termo, $field){
	$result = 0;
	foreach($obj as $ch => $d){

		if($d[$field] == $termo){
			$result++;
		}
	}

	return $result;
}

function turnoAtual($h){

	if($h >= '00:00:00' && $h <= '05:59:59'){
		return 1;
	}else if($h >= '06:00:00' && $h <= '11:59:59'){
		return 2;
	}else if($h >= '12:00:00' && $h <= '17:59:59'){
		return 3;
	}else{
		return 4;
	}
}

function enviarPush($msg,$devices){
    $content = array(
        "en" => $msg,
        "pt" => $msg
    );
    
    $fields = array(
        'app_id' => oneSignalAppId,
        'include_player_ids' => $devices,
        'data' => array("foo" => "bar"),
        'contents' => $content
    );
    
    $fields = json_encode($fields);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic '.restApiKey));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);
    
    return $response;
}

function qrCodeGenerator($sring){
	global $url_site;

	$aux = $url_site.'qrcodelib/qr_img.php?';
	$aux .= 'd='.$sring.'&';
	$aux .= 'e=H&';
	$aux .= 's=10&';
	$aux .= 't=J';

	return $aux;
}


include $rootInclude."phpqrcode/qrlib.php"; 
include $rootInclude."funcoes_pix.php";
function geraPix($chave,$descricao,$valorPedido,$razaoSocial,$cidade,$idPedido){
	$px[00]="01";
	$px[26][00]="BR.GOV.BCB.PIX";
	$px[26][01]=unMask($chave);
	$px[26][02]=$descricao;
	$px[52]="0000";
	$px[53]="986";
	$px[54]=moddesformat($valorPedido);
	$px[58]="BR";
	$px[59]=substr($razaoSocial,0,24);
	$px[60]=substr($cidade,0,14);
	$px[62][05]="***";
   	$px[62][50][00]="BR.GOV.BCB.BRCODE"; //Payment system specific template - GUI
   	$px[62][50][01]="1.0.0";
	// legivel($px); exit;
	$pix=montaPix($px);
	$pix.="6304";
	$pix.=crcChecksum($pix);

	ob_start();
	QRCode::png($pix, null,'M',5);
	$imageString = base64_encode( ob_get_contents() );
	ob_end_clean();
	// Exibe a imagem diretamente no navegador codificada em base64.
	return '<img src="data:image/png;base64,' . $imageString . '" width="70%">';
	// return $pix;
}

function geraPicpay($user,$valor){
	$dados = 'https://picpay.me/'.$user.'/'.$valor;
	return qrCodeGenerator($dados);
}

function unMask($d){
	switch (getTypeKeyPix($d)) {
		case 'CPF':
		case 'CNPJ':
			return str_replace(array('.','-',' ','(',')','/'),'',$d);
			break;
		case 'CELULAR':
			return str_replace(array('.','-',' ','(',')','/'),'',$d);
		default:
			return $d;
			break;
	}
}

function getTypeKeyPix($k){
	$len = strlen($k);
	$tip = strstr($k,'@');

	if($tip){
		return "E-MAIL";
	}else{
		switch ($len) {
			case 14:
				return 'CPF';
				break;
			case 19:
				return 'CELULAR';
				break;
			case 18:
				return 'CNPJ';
				break;
			default:
				return 'ALEATORIA';
				break;
		}
	}
}

function atualizaValorPedido($p){
	foreach($p as $ch => $item){

			$qtSabor = count($item['sabor']);
			$somaAdicionais = 0;
			$somaFracao = 0;
			if(!empty($item['tamanho'])){
				for ($s=1; $s <= $qtSabor ; $s++) {
					if(!empty($item['addFracao'][$s])){
						$somaFracao = $somaFracao + $item['addFracao'][$s];
					}

					$totalAdicionais = count($item['adicionais'][$s]);
					if($totalAdicionais > 0){
						
						for($a = 0; $a < $totalAdicionais; $a++){
							$somaAdicionais = $somaAdicionais + $item['adicionais'][$s][$a]['valor'];
							$somaItensAdicionais = $somaItensAdicionais + $item['adicionais'][$s][$a]['valor'];
						}

						$somaItensAdicionais = 0;
					}
				}
			}else{

				$totalAdicionais = count($item['adicionais']);
				if($totalAdicionais > 0){
					$somaAdicionais = 0;
					for($a = 0; $a < $totalAdicionais; $a++){
						$somaAdicionais = $somaAdicionais + $item['adicionais'][$a]['valor'];
						$somaItensAdicionais = $somaItensAdicionais + $item['adicionais'][$a]['valor'];
					}
					$somaItensAdicionais = 0;
				}					
			}
	
			$valor = $item['valorProduto'] + $item['valorBorda'] + $somaAdicionais + $somaFracao;
			

			if(count($item['promocao']) > 0){
				foreach($item['promocao'] as $promo){
					$totalPedido = $totalPedido + $promo['valor'];

				}
			}


		$totalPedido = $valor + $totalPedido;

	}	
	$_SESSION['PEDIDO_'.$_SESSION['idSession']]['totalPedido'] = $totalPedido;
}

function registraLog($local, $acao,$id){
	global $g, $user;

	$g->t('logs');
	$g->addValue('local',$local);
	$g->addValue('acao',$acao);
	$g->addValue('from_usuario',usuario('id','admin'));
	$g->addValue('from_id',$id);
	$g->redir = false;
	$g->add();
}

function calculoPrevisao($tf, $dp){
	global $status;

	$dataPedido = strtotime(dateTimeHiffen($dp,'/'));
	$dataAtual = strtotime(date("Y-m-d H:i:s"));

	if($tf == 1){
		$tempoEstimado = strtotime("+".$status['tempoDelivery']."minutes", $dataPedido);
		$txt = "Estimativa de entrega em ";
	}else{
		$tempoEstimado = strtotime("+".$status['tempoBalcao']."minutes", $dataPedido);
		$txt = "Estimativa de retirada em ";
	}
	$dataEstimada = new Datetime(date("Y-m-d H:i:s"));

	$tempoRestante = $dataEstimada->diff(new DateTime(date("Y-m-d H:i:s", $tempoEstimado)),true);

	if($tempoEstimado > $dataAtual){
		$zeroH = ($tempoRestante->h < 10 ? '0' : '');
		$zeroM = ($tempoRestante->i < 10 ? '0' : '');
		return $txt.($tempoRestante->h > 0 ? $zeroH.$tempoRestante->h.'h' : '').$zeroM.$tempoRestante->i."min.";
	}else{
		return "Pedido Atrasado";
	}
}

?>